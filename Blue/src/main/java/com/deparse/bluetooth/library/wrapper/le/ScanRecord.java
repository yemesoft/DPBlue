package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class ScanRecord {
    private final android.bluetooth.le.ScanRecord mOrigin;

    public ScanRecord(android.bluetooth.le.ScanRecord origin) {
        this.mOrigin = origin;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getAdvertiseFlags() {
        return mOrigin.getAdvertiseFlags();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public byte[] getBytes() {
        return mOrigin.getBytes();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public String getDeviceName() {
        return mOrigin.getDeviceName();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.util.SparseArray getManufacturerSpecificData() {
        return mOrigin.getManufacturerSpecificData();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public byte[] getManufacturerSpecificData(int arg0) {
        return mOrigin.getManufacturerSpecificData(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public java.util.Map getServiceData() {
        return mOrigin.getServiceData();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public byte[] getServiceData(android.os.ParcelUuid arg0) {
        return mOrigin.getServiceData(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public java.util.List getServiceUuids() {
        return mOrigin.getServiceUuids();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getTxPowerLevel() {
        return mOrigin.getTxPowerLevel();
    }


    public String toString() {
        return mOrigin.toString();
    }
}
