package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class BluetoothLeAdvertiser {
    private final android.bluetooth.le.BluetoothLeAdvertiser mOrigin;

    public BluetoothLeAdvertiser(android.bluetooth.le.BluetoothLeAdvertiser origin) {
        this.mOrigin = origin;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void startAdvertising(android.bluetooth.le.AdvertiseSettings arg0, android.bluetooth.le.AdvertiseData arg1, android.bluetooth.le.AdvertiseCallback arg2) {
        mOrigin.startAdvertising(arg0, arg1, arg2);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void startAdvertising(android.bluetooth.le.AdvertiseSettings arg0, android.bluetooth.le.AdvertiseData arg1, android.bluetooth.le.AdvertiseData arg2, android.bluetooth.le.AdvertiseCallback arg3) {
        mOrigin.startAdvertising(arg0, arg1, arg2, arg3);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startAdvertisingSet(android.bluetooth.le.AdvertisingSetParameters arg0, android.bluetooth.le.AdvertiseData arg1, android.bluetooth.le.AdvertiseData arg2, android.bluetooth.le.PeriodicAdvertisingParameters arg3, android.bluetooth.le.AdvertiseData arg4, android.bluetooth.le.AdvertisingSetCallback arg5) {
        mOrigin.startAdvertisingSet(arg0, arg1, arg2, arg3, arg4, arg5);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startAdvertisingSet(android.bluetooth.le.AdvertisingSetParameters arg0, android.bluetooth.le.AdvertiseData arg1, android.bluetooth.le.AdvertiseData arg2, android.bluetooth.le.PeriodicAdvertisingParameters arg3, android.bluetooth.le.AdvertiseData arg4, android.bluetooth.le.AdvertisingSetCallback arg5, android.os.Handler arg6) {
        mOrigin.startAdvertisingSet(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startAdvertisingSet(android.bluetooth.le.AdvertisingSetParameters arg0, android.bluetooth.le.AdvertiseData arg1, android.bluetooth.le.AdvertiseData arg2, android.bluetooth.le.PeriodicAdvertisingParameters arg3, android.bluetooth.le.AdvertiseData arg4, int arg5, int arg6, android.bluetooth.le.AdvertisingSetCallback arg7) {
        mOrigin.startAdvertisingSet(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startAdvertisingSet(android.bluetooth.le.AdvertisingSetParameters arg0, android.bluetooth.le.AdvertiseData arg1, android.bluetooth.le.AdvertiseData arg2, android.bluetooth.le.PeriodicAdvertisingParameters arg3, android.bluetooth.le.AdvertiseData arg4, int arg5, int arg6, android.bluetooth.le.AdvertisingSetCallback arg7, android.os.Handler arg8) {
        mOrigin.startAdvertisingSet(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void stopAdvertising(android.bluetooth.le.AdvertiseCallback arg0) {
        mOrigin.stopAdvertising(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void stopAdvertisingSet(android.bluetooth.le.AdvertisingSetCallback arg0) {
        mOrigin.stopAdvertisingSet(arg0);
    }

}
