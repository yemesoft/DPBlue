package com.deparse.bluetooth.library.blue;

import android.bluetooth.BluetoothDevice;

import java.io.IOException;
import java.util.UUID;

public interface BlueDeviceInterface {

    String getName();

    String getAddress();

    boolean createBond();

    boolean removeBond();

    int getBondState();

    BlueSocket createInsecureRfcommSocketToServiceRecord(UUID uuid) throws IOException;

    BlueSocket createRfcommSocketToServiceRecord(UUID uuid) throws IOException;

    BluetoothDevice getRealDevice();
}