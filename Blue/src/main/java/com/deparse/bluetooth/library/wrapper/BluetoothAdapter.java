package com.deparse.bluetooth.library.wrapper;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class BluetoothAdapter {
    private static final android.bluetooth.BluetoothAdapter mOrigin = android.bluetooth.BluetoothAdapter.getDefaultAdapter();

    private static BluetoothAdapter sAdapter = null;

    public boolean cancelDiscovery() {
        return mOrigin.cancelDiscovery();
    }


    public static boolean checkBluetoothAddress(String arg0) {
        return android.bluetooth.BluetoothAdapter.checkBluetoothAddress(arg0);
    }

    public void closeProfileProxy(int arg0, android.bluetooth.BluetoothProfile arg1) {
        mOrigin.closeProfileProxy(arg0, arg1);
    }


    public boolean disable() {
        return mOrigin.disable();
    }


    public boolean enable() {
        return mOrigin.enable();
    }


    public String getAddress() {
        return mOrigin.getAddress();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.bluetooth.le.BluetoothLeAdvertiser getBluetoothLeAdvertiser() {
        return mOrigin.getBluetoothLeAdvertiser();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.bluetooth.le.BluetoothLeScanner getBluetoothLeScanner() {
        return mOrigin.getBluetoothLeScanner();
    }


    public java.util.Set getBondedDevices() {
        return mOrigin.getBondedDevices();
    }


    public static BluetoothAdapter getDefaultAdapter() {
        if (null == sAdapter) {
            synchronized (BluetoothAdapter.class) {
                if (null == sAdapter) {
                    sAdapter = new BluetoothAdapter();
                }
            }
        }
        return sAdapter;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getLeMaximumAdvertisingDataLength() {
        return mOrigin.getLeMaximumAdvertisingDataLength();
    }


    public String getName() {
        return mOrigin.getName();
    }

    public int getProfileConnectionState(int arg0) {
        return mOrigin.getProfileConnectionState(arg0);
    }


    public boolean getProfileProxy(android.content.Context arg0, android.bluetooth.BluetoothProfile.ServiceListener arg1, int arg2) {
        return mOrigin.getProfileProxy(arg0, arg1, arg2);
    }


    public BluetoothDevice getRemoteDevice(String arg0) {
        return new BluetoothDevice(mOrigin.getRemoteDevice(arg0));
    }


    public BluetoothDevice getRemoteDevice(byte[] arg0) {
        return new BluetoothDevice(mOrigin.getRemoteDevice(arg0));
    }


    public int getScanMode() {
        return mOrigin.getScanMode();
    }


    public int getState() {
        return mOrigin.getState();
    }


    public boolean isDiscovering() {
        return mOrigin.isDiscovering();
    }


    public boolean isEnabled() {
        return mOrigin.isEnabled();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isLe2MPhySupported() {
        return mOrigin.isLe2MPhySupported();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isLeCodedPhySupported() {
        return mOrigin.isLeCodedPhySupported();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isLeExtendedAdvertisingSupported() {
        return mOrigin.isLeExtendedAdvertisingSupported();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isLePeriodicAdvertisingSupported() {
        return mOrigin.isLePeriodicAdvertisingSupported();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean isMultipleAdvertisementSupported() {
        return mOrigin.isMultipleAdvertisementSupported();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean isOffloadedFilteringSupported() {
        return mOrigin.isOffloadedFilteringSupported();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean isOffloadedScanBatchingSupported() {
        return mOrigin.isOffloadedScanBatchingSupported();
    }


    public android.bluetooth.BluetoothServerSocket listenUsingInsecureRfcommWithServiceRecord(String arg0, java.util.UUID arg1) throws java.io.IOException {
        return mOrigin.listenUsingInsecureRfcommWithServiceRecord(arg0, arg1);
    }


    public android.bluetooth.BluetoothServerSocket listenUsingRfcommWithServiceRecord(String arg0, java.util.UUID arg1) throws java.io.IOException {
        return mOrigin.listenUsingRfcommWithServiceRecord(arg0, arg1);
    }


    public boolean setName(String arg0) {
        return mOrigin.setName(arg0);
    }


    public boolean startDiscovery() {
        return mOrigin.startDiscovery();
    }


    public boolean startLeScan(android.bluetooth.BluetoothAdapter.LeScanCallback arg0) {
        return mOrigin.startLeScan(arg0);
    }


    public boolean startLeScan(java.util.UUID[] arg0, android.bluetooth.BluetoothAdapter.LeScanCallback arg1) {
        return mOrigin.startLeScan(arg0, arg1);
    }


    public void stopLeScan(android.bluetooth.BluetoothAdapter.LeScanCallback arg0) {
        mOrigin.stopLeScan(arg0);
    }
}
