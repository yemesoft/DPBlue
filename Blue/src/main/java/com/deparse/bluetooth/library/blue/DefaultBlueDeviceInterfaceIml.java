package com.deparse.bluetooth.library.blue;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DefaultBlueDeviceInterfaceIml implements BlueDeviceInterface {
    private final BluetoothDevice mRealDevice;

    public DefaultBlueDeviceInterfaceIml(BluetoothDevice device) {
        this.mRealDevice = device;
    }

    public static BlueDeviceInterface createFromIntent(Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        return new DefaultBlueDeviceInterfaceIml(device);
    }

    @Override
    public String getName() {
        return mRealDevice.getName();
    }

    @Override
    public String getAddress() {
        return mRealDevice.getAddress();
    }

    @Override
    public boolean createBond() {
        return mRealDevice.createBond();
    }

    @Override
    public boolean removeBond() {
        try {
            Method removeBond = mRealDevice.getClass().getMethod("removeBond");
            removeBond.setAccessible(true);
            return (Boolean) removeBond.invoke(mRealDevice);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            //TODO nothing
        }
        return false;
    }

    @Override
    public int getBondState() {
        return mRealDevice.getBondState();
    }

    @Override
    public BlueSocket createInsecureRfcommSocketToServiceRecord(UUID uuid) throws IOException {
        return new BlueSocket(new DefaultBlueSocketInterfaceIml(mRealDevice.createInsecureRfcommSocketToServiceRecord(uuid)));
    }

    @Override
    public BlueSocket createRfcommSocketToServiceRecord(UUID uuid) throws IOException {
        return new BlueSocket(new DefaultBlueSocketInterfaceIml(mRealDevice.createRfcommSocketToServiceRecord(uuid)));
    }

    @Override
    public BluetoothDevice getRealDevice() {
        return mRealDevice;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof DefaultBlueDeviceInterfaceIml)) {
            return false;
        }
        return mRealDevice.equals(((DefaultBlueDeviceInterfaceIml) obj).mRealDevice);
    }

    @NonNull
    @Override
    public String toString() {
        return mRealDevice.toString();
    }
}
