package com.deparse.bluetooth.library;

import androidx.annotation.NonNull;

public class ByteArray {
    private static final int BUFFER_SIZE = 1024;

    private byte[] data = null;
    private int size = 0;

    public void add(byte[] bytes) {
        if (null == data) {
            data = new byte[BUFFER_SIZE];
        }
        if (bytes.length + size > data.length) {
            byte[] temp = new byte[size];
            System.arraycopy(data, 0, temp, 0, size);
            data = new byte[BUFFER_SIZE * (1 + (bytes.length + size) / BUFFER_SIZE)];
            System.arraycopy(temp, 0, data, 0, size);
        }
        System.arraycopy(bytes, 0, data, size, bytes.length);
        size += bytes.length;
    }

    public byte get(int position) {
        if (position < 0 || position >= size) {
            throw new IndexOutOfBoundsException();
        }
        return data[position];
    }

    public byte[] get() {
        if (null == data || 0 == size) {
            return null;
        }
        byte[] result = new byte[size];
        System.arraycopy(data, 0, result, 0, size);
        return result;
    }

    public void trim(int from) {
        trim(from, size - 1);
    }

    public void trim(int from, int to) {
        if (from > to || from >= size || to < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (to >= size) {
            to = size - 1;
        }
        byte[] temp = new byte[to - from + 1];
        System.arraycopy(data, from, temp, 0, temp.length);
        data = temp;
        size = temp.length;
    }

    public int size() {
        return size;
    }

    public void clear() {
        size = 0;
        data = null;
    }

    public byte[] take(int from, int to) {
        if (from > to || from >= size || to < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (to >= size) {
            to = size - 1;
        }
        int takenSize = to - from + 1;
        byte[] result = BlueUtil.copy(data, 0, takenSize - 1);
        data = BlueUtil.copy(data, takenSize);
        size -= takenSize;
        return result;
    }

    @NonNull
    @Override
    public String toString() {
        byte[] temp = BlueUtil.copy(data, 0, size - 1);
        return String.valueOf(BlueUtil.bytesToHex(temp));
    }
}
