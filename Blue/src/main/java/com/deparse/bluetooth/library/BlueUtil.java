package com.deparse.bluetooth.library;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class BlueUtil {
    private static byte hexToByte(String inHex) {
        return (byte) Integer.parseInt(inHex, 16);
    }

    public static byte[] hexToBytes(String inHex) {
        inHex = inHex.trim().replaceAll("\\s*(0x)?", "");
        int hexlen = inHex.length();
        byte[] result;
        if (hexlen % 2 == 1) {
            //奇数
            hexlen++;
            result = new byte[(hexlen / 2)];
            inHex = "0" + inHex;
        } else {
            //偶数
            result = new byte[(hexlen / 2)];
        }
        int j = 0;
        for (int i = 0; i < hexlen; i += 2) {
            result[j] = hexToByte(inHex.substring(i, i + 2));
            j++;
        }
        return result;
    }

    public static String bytesToHex(byte[] bytes) {
        if (null == bytes) {
            return null;
        }
        return bytesToHex(bytes, 0, bytes.length - 1);
    }

    public static String bytesToHex(byte[] bytes, int from, int to) {
        if (null == bytes) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        from = Math.max(from, 0);
        to = Math.max(to, bytes.length - 1);
        for (int i = from; i <= to; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() < 2) {
                sb.append(0);
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static String byteToHex(byte b) {
        String hex = Integer.toHexString(b & 0xFF);
        return hex.length() < 2 ? ("0" + hex) : hex;
    }

    /**
     * 整数转换成字节数组 关键技术：ByteArrayOutputStream和DataOutputStream
     *
     * @param n 需要转换整数
     * @return
     */
    public static byte[] shortToBytes(short n) throws IOException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        DataOutputStream dataOut = new DataOutputStream(byteOut);
        dataOut.writeShort(n);
        byte[] result = byteOut.toByteArray();
        dataOut.close();
        byteOut.close();
        return result;
    }

    /**
     * 字节数组转换成整数 关键技术：ByteArrayInputStream和DataInputStream
     *
     * @param bytes 需要转换的字节数组
     * @return
     */
    public static short bytesToShort(byte[] bytes) throws IOException {
        ByteArrayInputStream byteInput = new ByteArrayInputStream(bytes);
        DataInputStream dataInput = new DataInputStream(byteInput);
        short result = dataInput.readShort();
        dataInput.close();
        byteInput.close();
        return result;
    }

    public static byte[] copy(byte[] bytes) {
        return copy(bytes, 0, bytes.length - 1);
    }

    public static byte[] copy(byte[] bytes, int from) {
        return copy(bytes, from, bytes.length - 1);
    }

    public static byte[] copy(byte[] src, int from, int to) {
        if (from >= src.length || to < from) {
            return null;
        }
        if (from < 0) {
            from = 0;
        }
        if (to >= src.length) {
            to = src.length - 1;
        }
        byte[] dst = new byte[to - from + 1];
        System.arraycopy(src, from, dst, 0, dst.length);
        return dst;
    }

    public static float bytesToFloat(byte[] bytes) {
        return bytesToFloat(bytes, 0);
    }

    @SuppressWarnings("all")
    public static float bytesToFloat(byte[] bytes, int index) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            DataInputStream dis = new DataInputStream(bis);
            if (index > 0) {
                dis.skip(index);
            }
            if (true) {
                float result = dis.readFloat();
                dis.close();
                bis.close();
                return result;
            }
        } catch (Exception e) {

        }
        int l;
        l = bytes[index + 0];
        l &= 0xff;
        l |= ((long) bytes[index + 1] << 8);
        l &= 0xffff;
        l |= ((long) bytes[index + 2] << 16);
        l &= 0xffffff;
        l |= ((long) bytes[index + 3] << 24);
        return Float.intBitsToFloat(l);
    }

    public static byte[] floatToBytes(float value) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeFloat(value);
            byte[] result = bos.toByteArray();
            dos.close();
            bos.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
