package com.deparse.bluetooth.library.wrapper;

import android.Manifest;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.RequiresFeature;
import androidx.annotation.RequiresPermission;

public class BluetoothDevice {
    private final android.bluetooth.BluetoothDevice mOrigin;

    public BluetoothDevice(android.bluetooth.BluetoothDevice origin) {
        this.mOrigin = origin;
    }

    public android.bluetooth.BluetoothGatt connectGatt(android.content.Context arg0, boolean arg1, android.bluetooth.BluetoothGattCallback arg2) {
        return mOrigin.connectGatt(arg0, arg1, arg2);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public android.bluetooth.BluetoothGatt connectGatt(android.content.Context arg0, boolean arg1, android.bluetooth.BluetoothGattCallback arg2, int arg3) {
        return mOrigin.connectGatt(arg0, arg1, arg2, arg3);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public android.bluetooth.BluetoothGatt connectGatt(android.content.Context arg0, boolean arg1, android.bluetooth.BluetoothGattCallback arg2, int arg3, int arg4) {
        return mOrigin.connectGatt(arg0, arg1, arg2, arg3, arg4);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public android.bluetooth.BluetoothGatt connectGatt(android.content.Context arg0, boolean arg1, android.bluetooth.BluetoothGattCallback arg2, int arg3, int arg4, android.os.Handler arg5) {
        return mOrigin.connectGatt(arg0, arg1, arg2, arg3, arg4, arg5);
    }


    public boolean createBond() {
        return mOrigin.createBond();
    }


    public android.bluetooth.BluetoothSocket createInsecureRfcommSocketToServiceRecord(java.util.UUID arg0) throws java.io.IOException {
        return mOrigin.createInsecureRfcommSocketToServiceRecord(arg0);
    }


    public android.bluetooth.BluetoothSocket createRfcommSocketToServiceRecord(java.util.UUID arg0) throws java.io.IOException {
        return mOrigin.createRfcommSocketToServiceRecord(arg0);
    }


    public int describeContents() {
        return mOrigin.describeContents();
    }


    public boolean equals(Object arg0) {
        return mOrigin.equals(arg0);
    }


    public boolean fetchUuidsWithSdp() {
        return mOrigin.fetchUuidsWithSdp();
    }


    public String getAddress() {
        return mOrigin.getAddress();
    }


    public android.bluetooth.BluetoothClass getBluetoothClass() {
        return mOrigin.getBluetoothClass();
    }


    public int getBondState() {
        return mOrigin.getBondState();
    }


    public String getName() {
        return mOrigin.getName();
    }


    public int getType() {
        return mOrigin.getType();
    }


    public android.os.ParcelUuid[] getUuids() {
        return mOrigin.getUuids();
    }


    public int hashCode() {
        return mOrigin.hashCode();
    }


    @RequiresPermission(Manifest.permission.BLUETOOTH_PRIVILEGED)
    public boolean setPairingConfirmation(boolean arg0) {
        return mOrigin.setPairingConfirmation(arg0);
    }


    public boolean setPin(byte[] arg0) {
        return mOrigin.setPin(arg0);
    }

    @Override
    @RequiresFeature(name = "Camera", enforcement = "")
    public String toString() {
        return mOrigin.toString();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
