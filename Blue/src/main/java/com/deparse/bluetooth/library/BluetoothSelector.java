package com.deparse.bluetooth.library;

import android.content.Context;
import android.content.Intent;

import com.deparse.bluetooth.library.blue.BlueDevice;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class BluetoothSelector {
    static final String EXTRA_CALLBACK_ID = "EXTRA_CALLBACK_ID";

    static final Map<String, Callback> callbackMap = new HashMap<>();

    public static void select(Context context, Callback callback) {
        String id = genId(callback);
        callbackMap.put(id, callback);
        Intent intent = new Intent(context, BluetoothListActivity.class);
        intent.putExtra(EXTRA_CALLBACK_ID, id);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private static String genId(Callback callback) {
        return String.format(Locale.getDefault(), "%d@%d", System.currentTimeMillis(), callback.hashCode());
    }

    public interface Callback {

        void onDeviceSelected(BlueDevice device);
    }
}
