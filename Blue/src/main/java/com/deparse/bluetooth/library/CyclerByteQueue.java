package com.deparse.bluetooth.library;

import androidx.annotation.NonNull;

public class CyclerByteQueue {
    private final byte[] data;
    private int head = 0, tail = 0;

    private int size = 0;

    public CyclerByteQueue() {
        this(512);
    }

    public CyclerByteQueue(int capacity) {
        data = new byte[capacity];
    }

    public int capacity() {
        return null == data ? 0 : data.length;
    }

    public int size() {
        return size;
    }

    public int head() {
        return head;
    }

    public int tail() {
        return tail;
    }

    public void put(byte bt) {
        if (size >= capacity()) {
            throw new RuntimeException("buffer overflow");
        }
        data[tail] = bt;
        tail = (tail + 1) % capacity();
        size++;
    }

    public void put(byte[] bytes) {
        put(bytes, 0, bytes.length);
    }

    public void put(byte[] bytes, int offset, int length) {
        if (size + length > capacity()) {
            throw new RuntimeException("buffer overflow");
        }

        if (tail + length <= capacity()) {
            System.arraycopy(bytes, offset, data, tail, length);
        } else {
            int consumed = capacity() - tail;
            System.arraycopy(bytes, offset, data, tail, consumed);
            System.arraycopy(bytes, offset + consumed, data, 0, length - consumed);
        }

        tail = (tail + length) % capacity();
        size += length;
    }

    public byte at(int offset) {
        if (offset >= size) {
            return -1;
        }
        return data[(head + offset) % capacity()];
    }

    public byte get() {
        if (size <= 0) {
            return -1;
        }
        byte result = data[head];
        head = (head + 1) % capacity();
        size--;
        return result;
    }

    public int get(byte[] out) {
        return get(out, out.length);
    }

    public int get(byte[] out, int length) {
        return get(out, 0, length);
    }

    public int get(byte[] out, int offset, int length) {
        if (0 == size) {
            return 0;
        }
        int result = copy(out, offset, length);
        head = (head + offset + result) % capacity();
        size -= result;
        return result;
    }

    public byte[] copy() {
        return copy(size);
    }

    public byte[] copy(int length) {
        return copy(0, length);
    }

    public byte[] copy(int offset, int length) {
        if (0 == size) {
            return null;
        }
        byte[] out = new byte[length];
        int count = copy(out, offset, length);
        return count > 0 ? out : null;
    }

    public int copy(byte[] out, int offset, int length) {
        if (0 == size || length > out.length) {
            return 0;
        }
        if (length > size) {
            length = size;
        }
        if (head + offset + length <= capacity()) {
            System.arraycopy(data, head + offset, out, 0, length);
        } else {
            int consumed = capacity() - head - offset;
            System.arraycopy(data, head + offset, out, 0, consumed);
            System.arraycopy(data, 0, out, consumed, length - consumed);
        }
        return length;
    }

    @NonNull
    @Override
    public String toString() {
        if (size <= 0) {
            return "";
        }
        byte[] result = new byte[size];
        int count = copy(result, 0, size);
        return 0 == count ? "" : String.valueOf(BlueUtil.bytesToHex(result, 0, count - 1));
    }

    public void skip(int offset) {
        if (offset > size) {
            throw new IndexOutOfBoundsException();
        }
        head = (head + offset) % capacity();
        size -= offset;
    }
}