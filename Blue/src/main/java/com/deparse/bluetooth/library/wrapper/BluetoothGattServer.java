package com.deparse.bluetooth.library.wrapper;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class BluetoothGattServer {
    private final android.bluetooth.BluetoothGattServer mOrigin;

    public BluetoothGattServer(android.bluetooth.BluetoothGattServer origin) {
        this.mOrigin = origin;
    }

    public boolean addService(android.bluetooth.BluetoothGattService arg0) {
        return mOrigin.addService(arg0);
    }


    public void cancelConnection(android.bluetooth.BluetoothDevice arg0) {
        mOrigin.cancelConnection(arg0);
    }


    public void clearServices() {
        mOrigin.clearServices();
    }


    public void close() {
        mOrigin.close();
    }


    public boolean connect(android.bluetooth.BluetoothDevice arg0, boolean arg1) {
        return mOrigin.connect(arg0, arg1);
    }


    public java.util.List getConnectedDevices() {
        return mOrigin.getConnectedDevices();
    }


    public int getConnectionState(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.getConnectionState(arg0);
    }


    public java.util.List getDevicesMatchingConnectionStates(int[] arg0) {
        return mOrigin.getDevicesMatchingConnectionStates(arg0);
    }


    public android.bluetooth.BluetoothGattService getService(java.util.UUID arg0) {
        return mOrigin.getService(arg0);
    }


    public java.util.List getServices() {
        return mOrigin.getServices();
    }


    public boolean notifyCharacteristicChanged(android.bluetooth.BluetoothDevice arg0, android.bluetooth.BluetoothGattCharacteristic arg1, boolean arg2) {
        return mOrigin.notifyCharacteristicChanged(arg0, arg1, arg2);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void readPhy(android.bluetooth.BluetoothDevice arg0) {
        mOrigin.readPhy(arg0);
    }


    public boolean removeService(android.bluetooth.BluetoothGattService arg0) {
        return mOrigin.removeService(arg0);
    }


    public boolean sendResponse(android.bluetooth.BluetoothDevice arg0, int arg1, int arg2, int arg3, byte[] arg4) {
        return mOrigin.sendResponse(arg0, arg1, arg2, arg3, arg4);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setPreferredPhy(android.bluetooth.BluetoothDevice arg0, int arg1, int arg2, int arg3) {
        mOrigin.setPreferredPhy(arg0, arg1, arg2, arg3);
    }
}
