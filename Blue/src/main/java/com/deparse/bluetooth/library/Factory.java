package com.deparse.bluetooth.library;

import com.deparse.bluetooth.library.blue.BlueSocket;

import java.io.IOException;

public interface Factory {

    boolean send(Blue blue, BlueSocket socket, Request request) throws IOException;

    boolean receive(Blue blue, BlueSocket socket, Callback globalCallback) throws IOException;
}
