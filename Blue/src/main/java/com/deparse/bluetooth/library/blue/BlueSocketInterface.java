package com.deparse.bluetooth.library.blue;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface BlueSocketInterface {
    InputStream getInputStream() throws IOException;

    OutputStream OutputStream() throws IOException;

    void close() throws IOException;

    boolean isConnected();

    void connect() throws IOException;
}
