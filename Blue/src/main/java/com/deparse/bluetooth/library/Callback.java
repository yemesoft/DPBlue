package com.deparse.bluetooth.library;

public interface Callback {

    boolean onResponse(byte what, byte[] packet);

    void onError(Throwable e);
}