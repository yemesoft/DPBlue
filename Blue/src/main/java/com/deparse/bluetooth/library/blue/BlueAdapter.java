package com.deparse.bluetooth.library.blue;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import androidx.annotation.Nullable;

public class BlueAdapter {

    public static final String ACTION_DISCOVERY_STARTED = BluetoothAdapter.ACTION_DISCOVERY_STARTED;
    public static final String ACTION_DISCOVERY_FINISHED = BluetoothAdapter.ACTION_DISCOVERY_FINISHED;
    public static final String ACTION_STATE_CHANGED = BluetoothAdapter.ACTION_STATE_CHANGED;
    public static final String ACTION_CONNECTION_STATE_CHANGED = BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED;
    public static final String EXTRA_STATE = BluetoothAdapter.EXTRA_STATE;
    public static final int STATE_OFF = BluetoothAdapter.STATE_OFF;

    private static final BluetoothAdapter sRealAdapter = BluetoothAdapter.getDefaultAdapter();
    private static BlueAdapter instance = null;

    private final List<BlueDevice> mBondedDevice = new ArrayList<BlueDevice>() {
        @Override
        public boolean add(BlueDevice device) {
            for (int i = size() - 1; i >= 0; i++) {
                BlueDevice item = get(i);
                if (item.getAddress().equals(device.getAddress())) {
                    remove(i);
                }
            }
            return super.add(device);
        }

        @Override
        public boolean remove(@Nullable Object o) {
            if (!(o instanceof BlueDevice)) {
                return false;
            }
            BlueDevice device = (BlueDevice) o;
            for (int i = size() - 1; i >= 0; i++) {
                BlueDeviceInterface item = get(i);
                if (item.getAddress().equals(device.getAddress())) {
                    super.remove(i);
                }
            }
            return true;
        }
    };

    public static BlueAdapter getDefaultAdapter() {
        if (null == sRealAdapter) {
            return null;
        }
        if (null == instance) {
            synchronized (BlueAdapter.class) {
                if (null == instance) {
                    instance = new BlueAdapter();
                }
            }
        }
        return instance;
    }

    public boolean isEnabled() {
        return null != sRealAdapter && sRealAdapter.isEnabled();
    }

    public boolean isDiscovering() {
        return null != sRealAdapter && sRealAdapter.isDiscovering();
    }

    public void startDiscovery() {
        if (null != sRealAdapter) {
            sRealAdapter.startDiscovery();
        }
    }

    public void cancelDiscovery() {
        if (null != sRealAdapter) {
            sRealAdapter.cancelDiscovery();
        }
    }

    public Set<BlueDevice> getBondedDevices() {
        Set<BlueDevice> set = new HashSet<BlueDevice>() {
            @Override
            public boolean contains(Object o) {
                if (o instanceof BlueDevice) {
                    for (BlueDevice device : this) {
                        if (device.getAddress().equals(((BlueDevice) o).getAddress())) {
                            return true;
                        }
                    }
                }
                return super.contains(o);
            }
        };
        if (null == sRealAdapter) {
            return set;
        }
        Set<BluetoothDevice> devices = sRealAdapter.getBondedDevices();
        if (null == devices) {
            return set;
        }
        for (BluetoothDevice device : devices) {
            set.add(BlueDevice.create(device));
        }
        for (BlueDevice device : mBondedDevice) {
            set.add(BlueDevice.create(device));
        }
        return set;
    }

    public void addBondedDevice(BlueDevice device) {
        mBondedDevice.add(device);
    }

    public void removeBondedDevice(BlueDevice device) {
        mBondedDevice.remove(device);
    }

    public void enable() {
        if (null != sRealAdapter) {
            sRealAdapter.enable();
        }
    }

    public void disable() {
        if (null != sRealAdapter) {
            sRealAdapter.disable();
        }
    }
}
