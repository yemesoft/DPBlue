package com.deparse.bluetooth.library.factories;


import com.deparse.bluetooth.library.Blue;
import com.deparse.bluetooth.library.Callback;
import com.deparse.bluetooth.library.Factory;
import com.deparse.bluetooth.library.Request;
import com.deparse.bluetooth.library.blue.BlueSocket;

import java.io.IOException;

@SuppressWarnings("all")
public class DefaultFactory implements Factory {

    @Override
    public boolean send(Blue blue, BlueSocket socket, Request request) throws IOException {
        if (null == socket || !socket.isConnected()) {
            return false;
        }
        socket.getOutputStream().write(request.cmd);
        socket.getOutputStream().flush();
        return true;
    }

    @Override
    public boolean receive(Blue blue, BlueSocket socket, Callback globalCallback) throws IOException {
        int count;
        while ((count = socket.getInputStream().available()) == 0) ;
        byte[] buffer = new byte[count];
        socket.getInputStream().read(buffer, 0, buffer.length);
        if (null != globalCallback) {
            globalCallback.onResponse((byte) 0, buffer);
        }
        return true;
    }
}