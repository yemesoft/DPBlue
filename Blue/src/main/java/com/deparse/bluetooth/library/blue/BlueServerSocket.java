package com.deparse.bluetooth.library.blue;

import java.io.IOException;

public final class BlueServerSocket implements BlueServerSocketInterface {
    private final BlueServerSocketInterface mBlueServerSocketInterface;

    public BlueServerSocket(BlueServerSocketInterface anInterface) {
        this.mBlueServerSocketInterface = anInterface;
    }

    public BlueSocket accept() throws IOException {
        return mBlueServerSocketInterface.accept();
    }

    @Override
    public void close() throws IOException {
        mBlueServerSocketInterface.close();
    }
}
