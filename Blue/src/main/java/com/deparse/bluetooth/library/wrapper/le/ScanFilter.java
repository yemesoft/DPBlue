package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class ScanFilter {
    private final android.bluetooth.le.ScanFilter mOrigin;

    public ScanFilter(android.bluetooth.le.ScanFilter origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    public boolean equals(Object arg0) {
        return mOrigin.equals(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public String getDeviceAddress() {
        return mOrigin.getDeviceAddress();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public String getDeviceName() {
        return mOrigin.getDeviceName();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public byte[] getManufacturerData() {
        return mOrigin.getManufacturerData();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public byte[] getManufacturerDataMask() {
        return mOrigin.getManufacturerDataMask();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getManufacturerId() {
        return mOrigin.getManufacturerId();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public byte[] getServiceData() {
        return mOrigin.getServiceData();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public byte[] getServiceDataMask() {
        return mOrigin.getServiceDataMask();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.os.ParcelUuid getServiceDataUuid() {
        return mOrigin.getServiceDataUuid();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.os.ParcelUuid getServiceUuid() {
        return mOrigin.getServiceUuid();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.os.ParcelUuid getServiceUuidMask() {
        return mOrigin.getServiceUuidMask();
    }


    public int hashCode() {
        return mOrigin.hashCode();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean matches(android.bluetooth.le.ScanResult arg0) {
        return mOrigin.matches(arg0);
    }


    public String toString() {
        return mOrigin.toString();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
