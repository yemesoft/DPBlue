package com.deparse.bluetooth.library.wrapper;

public class BluetoothGattDescriptor {
    private final android.bluetooth.BluetoothGattDescriptor mOrigin;

    public BluetoothGattDescriptor(android.bluetooth.BluetoothGattDescriptor origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    public android.bluetooth.BluetoothGattCharacteristic getCharacteristic() {
        return mOrigin.getCharacteristic();
    }


    public int getPermissions() {
        return mOrigin.getPermissions();
    }


    public java.util.UUID getUuid() {
        return mOrigin.getUuid();
    }


    public byte[] getValue() {
        return mOrigin.getValue();
    }


    public boolean setValue(byte[] arg0) {
        return mOrigin.setValue(arg0);
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
