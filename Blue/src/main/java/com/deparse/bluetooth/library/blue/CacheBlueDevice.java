package com.deparse.bluetooth.library.blue;

import android.bluetooth.BluetoothDevice;
import android.os.Looper;
import android.os.NetworkOnMainThreadException;

import com.deparse.bluetooth.library.Blue;
import com.deparse.bluetooth.library.BlueUtil;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class CacheBlueDevice implements BlueDeviceInterface {
    private final BlueDevice mDevice;
    private Interceptor mInterceptor = null;

    public CacheBlueDevice(BlueDevice device) {
        super();
        this.mDevice = device;
    }

    public void setInterceptor(Interceptor interceptor) {
        this.mInterceptor = interceptor;
    }

    @Override
    public String getName() {
        return mDevice.getName();
    }

    @Override
    public String getAddress() {
        return mDevice.getAddress();
    }

    @Override
    public boolean createBond() {
        return mDevice.createBond();
    }

    @Override
    public boolean removeBond() {
        return mDevice.removeBond();
    }

    @Override
    public int getBondState() {
        return mDevice.getBondState();
    }

    @Override
    public BlueSocket createInsecureRfcommSocketToServiceRecord(UUID uuid) throws IOException {
        return createSocket(uuid);
    }

    @Override
    public BlueSocket createRfcommSocketToServiceRecord(UUID uuid) throws IOException {
        return createSocket(uuid);
    }

    @Override
    public BluetoothDevice getRealDevice() {
        return mDevice.getRealDevice();
    }

    private BlueSocket createSocket(UUID uuid) throws IOException {
        return new BlueSocket(new BlueSocketInterfaceIml(CacheBlueDevice.this, mDevice, uuid));
    }

    private static class BlueSocketInterfaceIml implements BlueSocketInterface {

        private Socket mSocket;
        private Server mServer;

        public BlueSocketInterfaceIml(CacheBlueDevice cacheBlueDevice, BlueDevice device, UUID uuid) throws IOException {
            mServer = new Server(cacheBlueDevice, device, uuid);
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (null != mSocket) {
                return mSocket.getInputStream();
            }
            return null;
        }

        @Override
        public OutputStream OutputStream() throws IOException {
            if (null != mSocket) {
                return mSocket.getOutputStream();
            }
            return null;
        }

        @Override
        public void close() throws IOException {
            if (null != mSocket) {
                mSocket.close();
            }
            mServer.stopServer();
        }

        @Override
        public boolean isConnected() {
            if (null != mSocket) {
                return mSocket.isConnected();
            }
            return false;
        }

        @Override
        public void connect() throws IOException {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                throw new NetworkOnMainThreadException();
            }
            mServer.startServer();
            while (0 == mServer.getPort()) ;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            InetSocketAddress addr = new InetSocketAddress(mServer.getAddress(), mServer.getPort());
            this.mSocket = new Socket();
            mSocket.connect(addr);
        }
    }

    private static class Server {
        private final CacheBlueDevice mCacheBlueDevice;
        private String address = "127.0.0.1";
        private int port = 0;

        private final BlueDevice mBlueDevice;
        private final UUID mUUID;


        private boolean running = false;

        private final List<byte[]> cache = Collections.synchronizedList(new ArrayList<byte[]>());

        private BlueSocket mBlueSocket;

        public Server(CacheBlueDevice cacheBlueDevice, BlueDevice device, UUID uuid) {
            this.mCacheBlueDevice = cacheBlueDevice;
            this.mBlueDevice = device;
            this.mUUID = uuid;
        }

        public String getAddress() {
            return address;
        }

        public int getPort() {
            return port;
        }

        private void startServer() {
            stopServer();
            running = true;
            Blue.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.currentThread().setName("app=>device");
                        final ServerSocket serverSocket = new ServerSocket(0, 0, InetAddress.getByName(address));
                        port = serverSocket.getLocalPort();
                        while (running) {
                            final Socket socket = serverSocket.accept();
                            //从App端读取指令并发送给设备
                            Blue.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.currentThread().setName("app=>device");
                                        byte[] buffer = new byte[64];
                                        while (running && socket.isConnected()) {
                                            connectBlueSocket();
                                            int count = socket.getInputStream().read(buffer);
                                            if (count > 0) {
                                                if (null != mCacheBlueDevice.mInterceptor && mCacheBlueDevice.mInterceptor.interceptAppCmds(mBlueSocket, cache, buffer, count)) {
                                                    continue;
                                                }
                                                mBlueSocket.getOutputStream().write(buffer, 0, count);
                                                mBlueSocket.getOutputStream().flush();
                                            }
                                        }
                                        if (null != socket && !socket.isClosed()) {
                                            socket.close();
                                        }
                                    } catch (IOException e) {

                                    }
                                }
                            });
                            //将缓存中的数据发送给App
                            Blue.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.currentThread().setName("buffer=>app");
                                        while (running && socket.isConnected()) {
                                            final byte[] data;
                                            synchronized (cache) {
                                                data = cache.size() > 0 ? cache.remove(0) : null;
                                            }
                                            if (null != data) {
                                                if (null != mCacheBlueDevice.mInterceptor && mCacheBlueDevice.mInterceptor.interceptDeviceResponse(socket, data)) {
                                                    continue;
                                                }
                                                socket.getOutputStream().write(data);
                                                socket.getOutputStream().flush();
                                            }
                                        }
                                        if (null != socket && !socket.isClosed()) {
                                            socket.close();
                                        }
                                    } catch (IOException e) {
                                        try {
                                            socket.close();
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                }
                            });
                            //从设备读取数据存放到缓存中
                            Blue.execute(new Runnable() {
                                @Override
                                public void run() {
                                    Thread.currentThread().setName("device=>buffer");
                                    try {
                                        byte[] buffer = new byte[1024];
                                        while (running) {
                                            connectBlueSocket();
                                            int count = mBlueSocket.getInputStream().read(buffer);
                                            if (count > 0) {
                                                synchronized (cache) {
                                                    byte[] temp = Arrays.copyOf(buffer, count);
                                                    Blue.trace("real origin:" + BlueUtil.bytesToHex(temp));
                                                    cache.add(temp);
                                                }
                                            }
                                        }
                                        if (null != socket) {
                                            socket.close();
                                        }
                                    } catch (IOException e) {
                                        if (null != socket) {
                                            try {
                                                socket.close();
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            });
                        }
                        serverSocket.close();
                    } catch (IOException e) {

                    }
                }
            });
        }

        private void stopServer() {
            port = 0;
            running = false;
        }

        private synchronized void connectBlueSocket() throws IOException {
            while (null == mBlueSocket || !mBlueSocket.isConnected()) {
                try {
                    if (null != mBlueSocket) {
                        mBlueSocket.close();
                    }
                } finally {
                    mBlueSocket = null;
                }
                mBlueSocket = mBlueDevice.createRfcommSocketToServiceRecord(mUUID);
                mBlueSocket.connect();
                if (mBlueSocket.isConnected()) {
                    break;
                }
            }
        }
    }

    public interface Interceptor {

        boolean interceptAppCmds(BlueSocket socket, List<byte[]> cache, byte[] buffer, int count);

        boolean interceptDeviceResponse(Socket socket, byte[] data);
    }
}