package com.deparse.bluetooth.library;

import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.os.Looper;

import com.deparse.bluetooth.library.blue.BlueDevice;
import com.deparse.bluetooth.library.blue.BlueDeviceInterface;
import com.deparse.bluetooth.library.blue.BlueSocket;
import com.deparse.bluetooth.library.blue.BlueSocketInterface;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Arrays;
import java.util.UUID;
import java.util.WeakHashMap;

public abstract class Simulator extends BlueDevice {
    private static WeakHashMap<String, Simulator> Simulators = new WeakHashMap<>();
    private static Handler mMainHandler = new Handler(Looper.getMainLooper());

    @Override
    protected BlueDeviceInterface createDefaultBlueDeviceInterface() {
        return new SimulatorDeviceIml();
    }

    private class SimulatorDeviceIml implements BlueDeviceInterface {

        @Override
        public String getName() {
            return "SimulatorDevice";
        }

        @Override
        public String getAddress() {
            return "SimulatorDevice@" + Simulator.this.hashCode();
        }

        @Override
        public boolean createBond() {
            return true;
        }

        @Override
        public boolean removeBond() {
            return true;
        }

        @Override
        public int getBondState() {
            return BOND_BONDED;
        }

        @Override
        public BlueSocket createInsecureRfcommSocketToServiceRecord(UUID uuid) throws IOException {
            return createBlueSocket(uuid);
        }

        @Override
        public BlueSocket createRfcommSocketToServiceRecord(UUID uuid) throws IOException {
            return createBlueSocket(uuid);
        }

        @Override
        public BluetoothDevice getRealDevice() {
            throw new RuntimeException("");
        }

        private BlueSocket createBlueSocket(UUID uuid) throws IOException {
            return new BlueSocket(new PipeSocket(new ClientSocket()));
        }
    }

    private class PipeSocket implements BlueSocketInterface {
        private final PipedInputStream pis = new PipedInputStream();
        private final PipedOutputStream pos = new PipedOutputStream();
        private final ClientSocket mClientSocket;

        private boolean isConnected = false;

        public PipeSocket(ClientSocket serverSocket) {
            mClientSocket = serverSocket;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return pis;
        }

        @Override
        public OutputStream OutputStream() throws IOException {
            return pos;
        }

        public void close() throws IOException {
            try {
                mClientSocket.close();
            } catch (Exception e) {
                //TODO nothing
            }
            pis.close();
            pos.close();
            isConnected = false;
        }

        @Override
        public boolean isConnected() {
            return isConnected;
        }

        @Override
        public void connect() throws IOException {
            try {
                mClientSocket.close();
            } catch (Exception e) {
                //TODO nothing
            }
            pis.connect(mClientSocket.pos);
            pos.connect(mClientSocket.pis);
            isConnected = true;
        }
    }

    public class ClientSocket {
        private final PipedInputStream pis = new PipedInputStream();
        private final PipedOutputStream pos = new PipedOutputStream();

        ClientSocket() {
            new Thread() {
                @Override
                public void run() {
                    byte[] buffer = new byte[1024];
                    while (true) {
                        try {
                            int count = pis.read(buffer);
                            if (count > 0) {
                                Simulator.this.onReceived(ClientSocket.this, Arrays.copyOf(buffer, count));
                            }
                        } catch (final Exception e) {
                            mMainHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    throw new RuntimeException(e);
                                }
                            });
                            break;
                        }
                    }
                }
            }.start();
        }

        public void close() throws IOException {
            pis.close();
            pos.close();
        }

        public void write(byte[] bytes) throws IOException {
            pos.write(bytes);
        }

        public void flush() throws IOException {
            pos.flush();
        }
    }

    protected abstract void onReceived(ClientSocket socket, byte[] data);
}
