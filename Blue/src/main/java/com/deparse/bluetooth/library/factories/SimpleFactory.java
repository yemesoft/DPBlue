package com.deparse.bluetooth.library.factories;

import com.deparse.bluetooth.library.Blue;
import com.deparse.bluetooth.library.Callback;
import com.deparse.bluetooth.library.CyclerByteQueue;
import com.deparse.bluetooth.library.Request;
import com.deparse.bluetooth.library.Factory;
import com.deparse.bluetooth.library.BlueUtil;
import com.deparse.bluetooth.library.blue.BlueSocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 指令格式：
 * <p>
 * 起始标志          请求标识位	    指令长度    请求负载数据    结束标志
 * 1byte(0xA5)      2byte(xxxx)	    1byte      0-255byte      1byte(0xFA)
 */

/**
 * 响应格式：
 * <p>
 * 起始标志         请求标识位	    负载长度    请求负载数据    结束标志
 * 1byte(0xA5)      2byte(xxxx)	    1byte      0-255byte      1byte(0xFA)
 */
@SuppressWarnings("all")
public class SimpleFactory implements Factory {
    private static final int TIMEOUT_IN_MILLI_SECONDS = 1000;

    protected final Map<Byte, Request> requests = new HashMap<>();
    protected final Map<Byte, Request> requestsWatcher = new HashMap<>();

    protected final byte startByte;
    protected final byte endByte;

    protected final CyclerByteQueue mQueue = new CyclerByteQueue();

    protected final byte[] buffer = new byte[mQueue.capacity() / 2];

    public SimpleFactory() {
        this((byte) 0xa5, (byte) 0xfa);
    }

    public SimpleFactory(byte startByte, byte endByte) {
        this.startByte = startByte;
        this.endByte = endByte;
    }

    @Override
    public boolean send(final Blue blue, final BlueSocket socket, final Request request) throws IOException {
        performSend(blue, socket, request);
        return true;
    }

    private void performSend(final Blue blue, final BlueSocket socket, final Request request) throws IOException {
        if (null == socket || !socket.isConnected()) {
            if (null != request.callback) {
                request.callback.onError(new Throwable("socket is closed"));
            }
            return;
        }
        socket.getOutputStream().write(request.cmd);
        socket.getOutputStream().flush();

        final byte what = request.cmd[1];
        synchronized (requests) {
            requests.put(what, request);
        }
        synchronized (requestsWatcher) {
            requestsWatcher.put(what, request);
        }
        Blue.trace("Command sended:" + request);
        Blue.executeDelayed(new Runnable() {

            @Override
            public void run() {
                synchronized (requestsWatcher) {
                    if (requestsWatcher.containsKey(what)) {
                        synchronized (requests) {
                            if (requests.containsKey(what)) {
                                requests.remove(what);
                            }
                        }
                        requestsWatcher.remove(what);
                        if (null != request.callback) {
                            request.callback.onError(new TimeoutException("响应超时"));
                        }
                    }
                }
            }
        }, TIMEOUT_IN_MILLI_SECONDS);
    }

    @Override
    public boolean receive(Blue blue, BlueSocket socket, final Callback globalCallback) throws IOException {
        int count = socket.getInputStream().read(buffer, 0, buffer.length);
        if (count <= 0) {
            return false;
        }
        mQueue.put(buffer, 0, count);
        Blue.trace("origin:" + BlueUtil.bytesToHex(buffer, 0, count - 1));
        while (mQueue.size() > 0) {
            byte b = -1;
            int offset = -1;
            do {
                offset++;
                b = mQueue.at(offset);
            } while (-1 != b && startByte != b);
            if (startByte != b || mQueue.size() <= 0) {
                break;
            }

            if (offset > 0) {
                mQueue.skip(offset);
            }

            byte lengthb = mQueue.at(3);
            if (-1 == lengthb) {
                break;
            }
            int length = 0x00ff & lengthb;
            byte endb = mQueue.at(length + 4);
            if (-1 == endb) {
                break;
            }
            if (endByte != endb) {
                mQueue.skip(1);
                continue;
            }
            final byte[] packet = new byte[length + 5];
            Blue.trace("packet:" + BlueUtil.bytesToHex(packet));
            int result = mQueue.get(packet, length + 5);
            if (result != length + 5) {
                mQueue.skip(1);
                continue;
            }
            final byte whatFlag = packet[1];
            synchronized (requestsWatcher) {
                if (requestsWatcher.containsKey(whatFlag)) {
                    requestsWatcher.remove(whatFlag);
                }
            }
            synchronized (requests) {
                if (requests.containsKey(whatFlag)) {
                    final Request request = requests.get(whatFlag);
                    if (request.removeCallback) {
                        requests.remove(whatFlag);
                    }
                    if ((null == request.callback || !request.callback.onResponse(whatFlag, packet)) && null != globalCallback) {
                        globalCallback.onResponse(whatFlag, packet);
                    }
                } else {
                    if (null != globalCallback) {
                        globalCallback.onResponse(whatFlag, packet);
                    }
                }
            }
        }
        return true;
    }

    public Request removeCallback(Callback callback) {
        for (Map.Entry<Byte, Request> entry : requests.entrySet()) {
            if (callback.equals(entry.getValue().callback)) {
                return requests.remove(callback);
            }
        }
        return null;
    }

    public Callback removeCallback(byte what) {
        Request request = requests.remove(what);
        return null == request ? null : request.callback;
    }
}