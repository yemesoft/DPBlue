package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class BluetoothLeScanner {
    private final android.bluetooth.le.BluetoothLeScanner mOrigin;

    public BluetoothLeScanner(android.bluetooth.le.BluetoothLeScanner origin) {
        this.mOrigin = origin;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void flushPendingScanResults(android.bluetooth.le.ScanCallback arg0) {
        mOrigin.flushPendingScanResults(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void startScan(android.bluetooth.le.ScanCallback arg0) {
        mOrigin.startScan(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int startScan(java.util.List arg0, android.bluetooth.le.ScanSettings arg1, android.app.PendingIntent arg2) {
        return mOrigin.startScan(arg0, arg1, arg2);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void startScan(java.util.List arg0, android.bluetooth.le.ScanSettings arg1, android.bluetooth.le.ScanCallback arg2) {
        mOrigin.startScan(arg0, arg1, arg2);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void stopScan(android.app.PendingIntent arg0) {
        mOrigin.stopScan(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void stopScan(android.bluetooth.le.ScanCallback arg0) {
        mOrigin.stopScan(arg0);
    }
}
