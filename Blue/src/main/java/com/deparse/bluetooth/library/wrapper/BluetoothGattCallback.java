package com.deparse.bluetooth.library.wrapper;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class BluetoothGattCallback {
    private final android.bluetooth.BluetoothGattCallback mOrigin;

    public BluetoothGattCallback(android.bluetooth.BluetoothGattCallback origin) {
        this.mOrigin = origin;
    }

    public void onCharacteristicChanged(android.bluetooth.BluetoothGatt arg0, android.bluetooth.BluetoothGattCharacteristic arg1) {
        mOrigin.onCharacteristicChanged(arg0, arg1);
    }


    public void onCharacteristicRead(android.bluetooth.BluetoothGatt arg0, android.bluetooth.BluetoothGattCharacteristic arg1, int arg2) {
        mOrigin.onCharacteristicRead(arg0, arg1, arg2);
    }


    public void onCharacteristicWrite(android.bluetooth.BluetoothGatt arg0, android.bluetooth.BluetoothGattCharacteristic arg1, int arg2) {
        mOrigin.onCharacteristicWrite(arg0, arg1, arg2);
    }


    public void onConnectionStateChange(android.bluetooth.BluetoothGatt arg0, int arg1, int arg2) {
        mOrigin.onConnectionStateChange(arg0, arg1, arg2);
    }


    public void onDescriptorRead(android.bluetooth.BluetoothGatt arg0, android.bluetooth.BluetoothGattDescriptor arg1, int arg2) {
        mOrigin.onDescriptorRead(arg0, arg1, arg2);
    }


    public void onDescriptorWrite(android.bluetooth.BluetoothGatt arg0, android.bluetooth.BluetoothGattDescriptor arg1, int arg2) {
        mOrigin.onDescriptorWrite(arg0, arg1, arg2);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onMtuChanged(android.bluetooth.BluetoothGatt arg0, int arg1, int arg2) {
        mOrigin.onMtuChanged(arg0, arg1, arg2);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onPhyRead(android.bluetooth.BluetoothGatt arg0, int arg1, int arg2, int arg3) {
        mOrigin.onPhyRead(arg0, arg1, arg2, arg3);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onPhyUpdate(android.bluetooth.BluetoothGatt arg0, int arg1, int arg2, int arg3) {
        mOrigin.onPhyUpdate(arg0, arg1, arg2, arg3);
    }


    public void onReadRemoteRssi(android.bluetooth.BluetoothGatt arg0, int arg1, int arg2) {
        mOrigin.onReadRemoteRssi(arg0, arg1, arg2);
    }


    public void onReliableWriteCompleted(android.bluetooth.BluetoothGatt arg0, int arg1) {
        mOrigin.onReliableWriteCompleted(arg0, arg1);
    }


    public void onServicesDiscovered(android.bluetooth.BluetoothGatt arg0, int arg1) {
        mOrigin.onServicesDiscovered(arg0, arg1);
    }
}
