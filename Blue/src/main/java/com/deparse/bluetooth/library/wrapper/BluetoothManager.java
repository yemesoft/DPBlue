package com.deparse.bluetooth.library.wrapper;

public class BluetoothManager {
    private final android.bluetooth.BluetoothManager mOrigin;

    public BluetoothManager(android.bluetooth.BluetoothManager origin) {
        this.mOrigin = origin;
    }

    public android.bluetooth.BluetoothAdapter getAdapter() {
        return mOrigin.getAdapter();
    }


    public java.util.List getConnectedDevices(int arg0) {
        return mOrigin.getConnectedDevices(arg0);
    }


    public int getConnectionState(android.bluetooth.BluetoothDevice arg0, int arg1) {
        return mOrigin.getConnectionState(arg0, arg1);
    }


    public java.util.List getDevicesMatchingConnectionStates(int arg0, int[] arg1) {
        return mOrigin.getDevicesMatchingConnectionStates(arg0, arg1);
    }


    public android.bluetooth.BluetoothGattServer openGattServer(android.content.Context arg0, android.bluetooth.BluetoothGattServerCallback arg1) {
        return mOrigin.openGattServer(arg0, arg1);
    }
}
