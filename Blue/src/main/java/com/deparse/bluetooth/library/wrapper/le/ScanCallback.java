package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class ScanCallback {
    private final android.bluetooth.le.ScanCallback mOrigin;

    public ScanCallback(android.bluetooth.le.ScanCallback origin) {
        this.mOrigin = origin;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onBatchScanResults(java.util.List arg0) {
        mOrigin.onBatchScanResults(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onScanFailed(int arg0) {
        mOrigin.onScanFailed(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onScanResult(int arg0, android.bluetooth.le.ScanResult arg1) {
        mOrigin.onScanResult(arg0, arg1);
    }
}
