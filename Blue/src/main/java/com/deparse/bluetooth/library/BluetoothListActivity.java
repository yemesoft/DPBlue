package com.deparse.bluetooth.library;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.deparse.bluetooth.library.blue.BlueDevice;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class BluetoothListActivity extends AppCompatActivity implements Blue.DiscoveryCallback, BluetoothListFragment.OnDeviceSelectedListener {
    protected ImageView refreshButton;
    private String callbackId = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blue_activity_bluetooth_list);

        final BluetoothListFragment fragment = new BluetoothListFragment();

        findViewById(R.id.iv_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        refreshButton = (ImageView) findViewById(R.id.refresh_button);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                fragment.refresh();
            }
        });

        fragment.setDiscoveryCallback(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

        fragment.setOnDeviceSelectedListener(this);

        callbackId = getIntent().getStringExtra(BluetoothSelector.EXTRA_CALLBACK_ID);
    }

    private void startAnimation() {
        stopAnimation();
        ValueAnimator animation = ValueAnimator.ofFloat(360, 0);
        animation.setRepeatCount(-1);
        animation.setRepeatMode(ValueAnimator.RESTART);
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(1000);
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                refreshButton.setRotation((Float) animation.getAnimatedValue());
            }
        });
        animation.start();
        refreshButton.setTag(animation);
    }

    private void stopAnimation() {
        if (null != refreshButton.getTag()) {
            ValueAnimator animation = (ValueAnimator) refreshButton.getTag();
            animation.cancel();
            refreshButton.setTag(null);
            refreshButton.setRotation(0);
        }
    }

    @Override
    public void onDiscoveryStarted() {
        startAnimation();
    }

    @Override
    public void onDiscoveryFinished() {
        stopAnimation();
    }

    @Override
    public void onDeviceFound(BlueDevice device) {

    }

    @Override
    public void onDeviceSelected(BlueDevice device) {
        BluetoothSelector.Callback callback = BluetoothSelector.callbackMap.get(callbackId);
        if (null != callback) {
            callback.onDeviceSelected(device);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        BluetoothSelector.callbackMap.remove(callbackId);
        super.onDestroy();
    }
}