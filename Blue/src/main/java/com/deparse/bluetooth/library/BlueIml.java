package com.deparse.bluetooth.library;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.widget.Toast;

import com.deparse.bluetooth.library.blue.BlueAdapter;
import com.deparse.bluetooth.library.blue.BlueDevice;
import com.deparse.bluetooth.library.blue.BlueSocket;
import com.deparse.bluetooth.library.factories.DefaultFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;

class BlueIml extends Blue {

    private final String mUuid;
    private final Context mContext;
    private final BlueDevice mDevice;
    private final boolean mInsecure;

    private volatile Callback mGlobalCallback = null;
    private volatile ConnectionCallback mConnectionCallback = new ConnectionCallbackWrapper();

    private volatile BlueSocket mSocket = null;

    private final List<Factory> mFactories = new ArrayList<>();

    private volatile ArrayBlockingQueue<Request> commandsQueue = new ArrayBlockingQueue<>(5);
    private volatile Processor mProcessor = new ProcessorWrapper();

    public BlueIml(Context context, BlueDevice device, String uuid, boolean insecure) {
        this.mContext = context.getApplicationContext();
        this.mDevice = device;
        this.mUuid = uuid;
        this.mInsecure = insecure;
        mFactories.add(new DefaultFactory());
    }

    @Override
    public void setConnectionCallback(ConnectionCallback callback) {
        mConnectionCallback = new ConnectionCallbackWrapper(callback);
    }

    @Override
    public void setGlobalCallback(Callback callback) {
        mGlobalCallback = callback;
    }

    @Override
    public void setProcessor(@NonNull final Processor processor) {
        mProcessor = new ProcessorWrapper(processor);
    }

    @Override
    public void setFactory(@NonNull Factory factory) {
        Objects.requireNonNull(factory);
        mFactories.clear();
        mFactories.add(new DefaultFactory());
        mFactories.add(0, factory);
    }

    @Override
    public void addFactory(Factory factory) {
        mFactories.add(0, factory);
    }

    @Override
    public void removeFactory(Factory factory) {
        mFactories.remove(factory);
    }

    @Override
    protected void onStateChanging(int lastState, int nowState, boolean notify) {

    }

    @Override
    protected void onStateChanged(int lastState, int state, boolean notify) {
        switch (state) {
            case STATE_CONNECTING:
                sAdapter.cancelDiscovery();
                if (notify) {
                    mConnectionCallback.onConnecting(mDevice, mUuid);
                }
                break;
            case STATE_CONNECTED:
                if (notify) {
                    mConnectionCallback.onConnected(mDevice, mUuid);
                }
                break;
            case STATE_DISCONNECTING:
                if (notify) {
                    mConnectionCallback.onDisonnecting(mDevice, mUuid);
                }
                break;
            case STATE_DISCONNECTED:
                if (null != mSocket) {
                    try {
                        mSocket.getInputStream().close();
                    } catch (Exception e) {
                    }
                    try {
                        mSocket.getOutputStream().close();
                    } catch (Exception e) {
                    }
                    try {
                        mSocket.close();
                    } catch (Exception e) {
                    }
                    mSocket = null;
                }
                if (notify) {
                    mConnectionCallback.onDisonnected(mDevice, mUuid);
                }
                break;
            case STATE_CONNECT_FAILURE:
                if (notify) {
                    mConnectionCallback.onConnectFailure(mDevice, mUuid);
                }
                setState(STATE_DISCONNECTED, false);
                break;
            case STATE_DISCONNECT_FAILURE:
                if (notify) {
                    mConnectionCallback.onDisconnectFailure(mDevice, mUuid);
                }
                setState(STATE_CONNECTED, false);
                break;
        }
    }

    @Override
    public void connect() {
        final int lastState = getState();
        if (STATE_CONNECTED == getState()) {
            mConnectionCallback.onConnected(mDevice, mUuid);
            return;
        }
        if (STATE_CONNECTING == getState()) {
            return;
        }
        if (null == sAdapter) {
            Toast.makeText(mContext, "本机不支持蓝牙", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!sAdapter.isEnabled()) {
            Toast.makeText(mContext, "请打开蓝牙", Toast.LENGTH_SHORT).show();
            return;
        }
        setState(STATE_CONNECTING);
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                if (BlueDevice.BOND_BONDED == mDevice.getBondState()) {
                    makeConnection(lastState);
                } else {
                    mBondWatcher.setTag(lastState);
                    mBondWatcher.register(mContext, BlueDevice.ACTION_BOND_STATE_CHANGED);
                    mDevice.createBond();
                }
            }
        });
    }

    private void makeConnection(final int lastState) {
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (null != mSocket && mSocket.isConnected()) {
                        mSocket.close();
                    }
                    while (null != mSocket && mSocket.isConnected()) {
                        // Waiting for disconnected...
                    }
                    if (mInsecure) {
                        mSocket = mDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString(mUuid));
                    } else {
                        mSocket = mDevice.createRfcommSocketToServiceRecord(UUID.fromString(mUuid));
                    }
                    if (null != mSocket) {
                        if (mSocket.isConnected()) {
                            mSocket.close();
                            while (null != mSocket && mSocket.isConnected()) {
                                // Waiting for disconnected...
                            }
                        }
                        mSocket.connect();
                    }
                } catch (IOException e) {
                    Blue.trace(e);
                }
                if (null == mSocket || !mSocket.isConnected()) {
                    setState(STATE_CONNECT_FAILURE);
                    return;
                }
                setState(STATE_CONNECTED);
                final CountDownLatch latch = new CountDownLatch(2);
                //数据读取线程
                mExecutorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        latch.countDown();
                        try {
                            while (STATE_CONNECTED == BlueIml.this.getState() && mSocket.isConnected()) {
                                System.out.println("Buffer Reading...");
                                for (Factory mFactory : mFactories) {
                                    if (mFactory.receive(BlueIml.this, mSocket, mGlobalCallback)) {
                                        break;
                                    }
                                }
                            }
                        } catch (IOException e) {
                            Blue.trace(e);
                        }
                        setState(STATE_DISCONNECTED);
                    }
                });
                //数据写入线程
                mExecutorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        latch.countDown();
                        try {
                            while (STATE_CONNECTED == BlueIml.this.getState()) {
                                Request request = commandsQueue.take();
                                for (int i = 0, size = mFactories.size(); i < size; i++) {
                                    Factory factory = mFactories.get(i);
                                    if (factory.send(BlueIml.this, mSocket, request)) {
                                        break;
                                    }
                                }
                                Thread.sleep(1000);
                            }
                        } catch (IOException | InterruptedException e) {
                            Blue.trace(e);
                        }
                        setState(STATE_DISCONNECTED);
                    }
                });
                try {
                    latch.await(2, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                }
                mDisconnectWatcher.register(mContext);
            }
        });
    }

    @Override
    public void disconnect() {
        final int lastState = getState();
        if (STATE_DISCONNECTED == getState()) {
            mConnectionCallback.onDisonnected(mDevice, mUuid);
            return;
        }
        if (null == sAdapter) {
            Toast.makeText(mContext, "本机不支持蓝牙", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!sAdapter.isEnabled()) {
            return;
        }
        if (STATE_DISCONNECTING == getState()) {
            return;
        }
        setState(STATE_DISCONNECTING);
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (null != mSocket) {
                        if (mSocket.isConnected()) {
                            mSocket.close();
                        }
                        while (mSocket.isConnected()) ;
                        mSocket = null;
                    }
                } catch (Exception e) {
                } finally {
                    if (null != mSocket && mSocket.isConnected()) {
                        setState(STATE_DISCONNECT_FAILURE);
                    }
                }
            }
        });
    }

    @Override
    public void send(byte[] cmd) {
        send(cmd, null);
    }

    @Override
    public void send(final byte[] cmd, final Callback callback) {
        send(cmd, true, callback);
    }

    @Override
    public void send(final byte[] cmd, final boolean removeCallback, final Callback callback) {
        if (null == mSocket || !mSocket.isConnected()) {
            callback.onError(new Throwable("Socket is closed"));
            return;
        }
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    commandsQueue.put(new Request(mProcessor.process(cmd), removeCallback, callback));
                } catch (InterruptedException e) {
                    callback.onError(e);
                }
            }
        });
    }

    public void send(String cmd) {
        send(cmd, null);
    }

    @Override
    public void send(String cmd, Callback callback) {
        send(cmd, true, callback);
    }

    public void send(String cmd, boolean removeCallback, Callback callback) {
        byte[] cmdByte = mProcessor.process(cmd);
        send(cmdByte, removeCallback, callback);
    }

    @Override
    public BlueDevice getDevice() {
        return mDevice;
    }

    class ConnectionCallbackWrapper implements ConnectionCallback {

        private final ConnectionCallback mCallback;

        public ConnectionCallbackWrapper() {
            this(null);
        }

        public ConnectionCallbackWrapper(ConnectionCallback callback) {
            this.mCallback = callback;
        }

        @Override
        public void onConnecting(final BlueDevice device, final String uuid) {
            if (null != mCallback) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    mCallback.onConnecting(device, uuid);
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onConnecting(device, uuid);
                        }
                    });
                }
            }
        }

        @Override
        public void onConnected(final BlueDevice device, final String uuid) {
            if (null != mCallback) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    mCallback.onConnected(device, uuid);
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onConnected(device, uuid);
                        }
                    });
                }
            }
        }

        @Override
        public void onConnectFailure(final BlueDevice device, final String uuid) {
            if (null != mCallback) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    mCallback.onConnectFailure(device, uuid);
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onConnectFailure(device, uuid);
                        }
                    });
                }
            }
        }

        @Override
        public void onDisonnecting(final BlueDevice device, final String uuid) {
            if (null != mCallback) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    mCallback.onDisonnecting(device, uuid);
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onDisonnecting(device, uuid);
                        }
                    });
                }
            }
        }

        @Override
        public void onDisonnected(final BlueDevice device, final String uuid) {
            if (null != mCallback) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    mCallback.onDisonnected(device, uuid);
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onDisonnected(device, uuid);
                        }
                    });
                }
            }
        }

        @Override
        public void onDisconnectFailure(final BlueDevice device, final String uuid) {
            if (null != mCallback) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    mCallback.onDisconnectFailure(device, uuid);
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onDisconnectFailure(device, uuid);
                        }
                    });
                }
            }
        }
    }

    private DisconnectWatcher mDisconnectWatcher = new DisconnectWatcher();

    private class DisconnectWatcher extends TagBroadcastReceiver {

        @Override
        protected String[] getInnerActions() {
            return new String[]{BlueDevice.ACTION_ACL_DISCONNECTED, BlueAdapter.ACTION_STATE_CHANGED, BlueAdapter.ACTION_CONNECTION_STATE_CHANGED};
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (null == intent) {
                return;
            }
            if (STATE_DISCONNECTED == getState()) {
                return;
            }
            if (BlueAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
                int state = intent.getIntExtra(BlueAdapter.EXTRA_STATE, BlueAdapter.STATE_OFF);
                if (BlueAdapter.STATE_OFF == state) {
                    setState(STATE_DISCONNECTED);
                    unregister(context);
                }
            } else if (BlueAdapter.ACTION_CONNECTION_STATE_CHANGED.equals(intent.getAction()) || BlueDevice.ACTION_ACL_DISCONNECTED.equals(intent.getAction())) {
                BlueDevice device = BlueDevice.fromIntent(intent);
                if (mDevice.equals(device)) {
                    setState(STATE_DISCONNECTED);
                    unregister(context);
                }
            }
        }
    }

    private BondWatcher mBondWatcher = new BondWatcher();

    private class BondWatcher extends TagBroadcastReceiver {
        @Override
        protected String[] getInnerActions() {
            return super.getInnerActions();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (null == intent) {
                return;
            }
            BlueDevice device = BlueDevice.fromIntent(intent);
            if (null == device) {
                return;
            }
            int state = intent.getIntExtra(BlueDevice.EXTRA_BOND_STATE, BlueDevice.BOND_NONE);
            if (device.equals(mDevice)) {
                if (BlueDevice.BOND_BONDED == state) {
                    Integer lastState = getTag();
                    if (null != lastState) {
                        makeConnection(lastState);
                    }
                } else if (BlueDevice.BOND_NONE == state) {
                    unregister(context);
                    setState(STATE_CONNECT_FAILURE);
                }
            }
        }
    }

    class ProcessorWrapper implements Processor {

        private final Processor mProcessor;

        ProcessorWrapper() {
            this(null);
        }

        ProcessorWrapper(Processor processor) {
            this.mProcessor = processor;
        }

        @Override
        public byte[] process(String cmd) {
            if (null == mProcessor) {
                return cmd.getBytes();
            }
            return mProcessor.process(cmd);
        }

        @Override
        public byte[] process(byte[] cmd) {
            if (null == mProcessor) {
                return cmd;
            }
            return mProcessor.process(cmd);
        }
    }
}