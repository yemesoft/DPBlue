package com.deparse.bluetooth.library;

public interface Processor {
    byte[] process(String cmd);

    byte[] process(byte[] cmd);
}
