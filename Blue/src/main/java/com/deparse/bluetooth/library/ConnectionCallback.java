package com.deparse.bluetooth.library;

import com.deparse.bluetooth.library.blue.BlueDevice;

public interface ConnectionCallback {

    void onConnecting(BlueDevice device, String uuid);

    void onConnected(BlueDevice device, String uuid);

    void onConnectFailure(BlueDevice device, String uuid);

    void onDisonnecting(BlueDevice device, String uuid);

    void onDisonnected(BlueDevice device, String uuid);

    void onDisconnectFailure(BlueDevice device, String uuid);
}