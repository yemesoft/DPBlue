package com.deparse.bluetooth.library.queue;

import java.security.InvalidParameterException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ByteArrayBlockingQueue implements ByteQueue {
    private byte[] items;
    private int size = 0, putIndex = 0, takeIndex = 0;

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition notFull = lock.newCondition(), notEmpty = lock.newCondition();

    public ByteArrayBlockingQueue() {
        this(512);
    }

    public ByteArrayBlockingQueue(int capacity) {
        if (capacity <= 0) {
            throw new InvalidParameterException("capacity > 0 required");
        }
        items = new byte[capacity];
    }

    public int capacity() {
        return null != items ? items.length : 0;
    }

    @Override
    public void put(byte[] bytes, int offset, int length) throws InterruptedException {
        final ReentrantLock lock = this.lock;
        try {
            lock.lockInterruptibly();
            while (size == items.length) {
                notFull.await();
            }
            int consumed = items.length - size;
            if (length > consumed) {
                put(bytes, offset, consumed);
                put(bytes, offset + consumed, length - consumed);
            } else {
                System.arraycopy(bytes, offset, items, putIndex, length);
                putIndex = (putIndex + length) % capacity();
                notEmpty.signalAll();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void take(byte[] out, int offset, int length) throws InterruptedException {
        final ReentrantLock lock = this.lock;
        try {
            lock.lockInterruptibly();
            while (0 == size) {
                notEmpty.await();
            }
            if (length > size) {
                take(out, offset, size);
                take(out, offset, length - size);
            } else {
                int consumed = items.length - takeIndex;
                if (offset + length >= consumed) {
                    take(out, offset, 0, consumed);
                    take(out, 0, consumed, length - consumed);
                } else {

                }
            }
        } finally {
            lock.unlock();
        }
    }

    public void take(byte[] out, int srcOffset, int dstOffset, int length) {

    }

    @Override
    public void at(int offset) {

    }

    @Override
    public void skip(int offset) {

    }
}
