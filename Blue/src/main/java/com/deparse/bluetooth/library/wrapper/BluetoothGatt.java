package com.deparse.bluetooth.library.wrapper;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class BluetoothGatt {
    private final android.bluetooth.BluetoothGatt mOrigin;

    public BluetoothGatt(android.bluetooth.BluetoothGatt origin) {
        this.mOrigin = origin;
    }

    public void abortReliableWrite() {
        mOrigin.abortReliableWrite();
    }


    public void abortReliableWrite(android.bluetooth.BluetoothDevice arg0) {
        mOrigin.abortReliableWrite(arg0);
    }


    public boolean beginReliableWrite() {
        return mOrigin.beginReliableWrite();
    }


    public void close() {
        mOrigin.close();
    }


    public boolean connect() {
        return mOrigin.connect();
    }


    public void disconnect() {
        mOrigin.disconnect();
    }

    public boolean discoverServices() {
        return mOrigin.discoverServices();
    }


    public boolean executeReliableWrite() {
        return mOrigin.executeReliableWrite();
    }


    public java.util.List getConnectedDevices() {
        return mOrigin.getConnectedDevices();
    }


    public int getConnectionState(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.getConnectionState(arg0);
    }


    public android.bluetooth.BluetoothDevice getDevice() {
        return mOrigin.getDevice();
    }


    public java.util.List getDevicesMatchingConnectionStates(int[] arg0) {
        return mOrigin.getDevicesMatchingConnectionStates(arg0);
    }


    public android.bluetooth.BluetoothGattService getService(java.util.UUID arg0) {
        return mOrigin.getService(arg0);
    }


    public java.util.List getServices() {
        return mOrigin.getServices();
    }


    public boolean readCharacteristic(android.bluetooth.BluetoothGattCharacteristic arg0) {
        return mOrigin.readCharacteristic(arg0);
    }


    public boolean readDescriptor(android.bluetooth.BluetoothGattDescriptor arg0) {
        return mOrigin.readDescriptor(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void readPhy() {
        mOrigin.readPhy();
    }


    public boolean readRemoteRssi() {
        return mOrigin.readRemoteRssi();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean requestConnectionPriority(int arg0) {
        return mOrigin.requestConnectionPriority(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean requestMtu(int arg0) {
        return mOrigin.requestMtu(arg0);
    }


    public boolean setCharacteristicNotification(android.bluetooth.BluetoothGattCharacteristic arg0, boolean arg1) {
        return mOrigin.setCharacteristicNotification(arg0, arg1);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setPreferredPhy(int arg0, int arg1, int arg2) {
        mOrigin.setPreferredPhy(arg0, arg1, arg2);
    }


    public boolean writeCharacteristic(android.bluetooth.BluetoothGattCharacteristic arg0) {
        return mOrigin.writeCharacteristic(arg0);
    }


    public boolean writeDescriptor(android.bluetooth.BluetoothGattDescriptor arg0) {
        return mOrigin.writeDescriptor(arg0);
    }
}
