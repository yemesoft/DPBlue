package com.deparse.bluetooth.library.blue;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BlueSocket {
    private final BlueSocketInterface mRealBlueSocketInterface;

    public BlueSocket(BlueSocketInterface anInterface) {
        mRealBlueSocketInterface = anInterface;
    }

    public InputStream getInputStream() throws IOException {
        return mRealBlueSocketInterface.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        return mRealBlueSocketInterface.OutputStream();
    }

    public void close() throws IOException {
        mRealBlueSocketInterface.close();
    }

    public boolean isConnected() {
        return mRealBlueSocketInterface.isConnected();
    }

    public void connect() throws IOException {
        mRealBlueSocketInterface.connect();
    }
}