package com.deparse.bluetooth.library;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.deparse.bluetooth.library.blue.BlueAdapter;
import com.deparse.bluetooth.library.blue.BlueDevice;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

@SuppressWarnings("all")
public abstract class Blue {
    public static final int STATE_CONNECTED = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_DISCONNECTED = 3;
    public static final int STATE_DISCONNECTING = 4;

    public static final int STATE_CONNECT_FAILURE = 5;
    public static final int STATE_DISCONNECT_FAILURE = 6;

    private static Context sContext;

    private static boolean isDebug = true;
    private static String sTag = "Blue";

    public static boolean isDebug() {
        return isDebug;
    }

    public static void setDebug(boolean debug) {
        Blue.isDebug = debug;
    }

    public static void setTag(String tag) {
        Blue.sTag = tag;
    }

    public static void info(String fmt, Object... params) {
        if (!isDebug()) {
            return;
        }
        System.out.println(makeMessage(fmt, params));
    }

    public static void error(String fmt, Object... params) {
        if (!isDebug()) {
            return;
        }
        System.err.println(makeMessage(fmt, params));
    }

    public static void trace(Object... params) {
        if (!isDebug()) {
            return;
        }
        String msg = "TRACE:";
        if (null != params && params.length > 0) {
            for (Object item : params) {
                msg += (String.valueOf(item) + ",");
            }
        }
        msg += Thread.currentThread().getStackTrace()[3];
        info(msg);
    }

    private static String makeMessage(String fmt, Object... params) {
        return sTag + ":" + String.format(Locale.getDefault(), fmt, params);
    }

    public static void executeDelay(Runnable runnable, long delay) {

    }

    public static void execute(@NonNull final Runnable runnable) {
        if (null != runnable) {
            mExecutorService.execute(runnable);
        }
    }

    public static void executeDelayed(@NonNull final Runnable runnable, final int delay) {
        if (null == runnable) {
            return;
        }
        if (delay <= 0) {
            mExecutorService.execute(runnable);
        }
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                }
                runnable.run();
            }
        });
    }

    public static void enable() {
        enable(null);
    }

    public static void enable(final Callback callback) {
        if (null == callback) {
            if (null != sAdapter) {
                sAdapter.enable();
            }
            return;
        }
        Blue.execute(new Runnable() {
            @Override
            public void run() {
                if (null == sAdapter) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(new Throwable("Bluetooth not supported"));
                        }
                    });
                    return;
                }
                sAdapter.enable();
                int count = 0;
                while (count++ < 5 && !sAdapter.isEnabled()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (sAdapter.isEnabled()) {
                            callback.onResponse(Byte.MIN_VALUE, null);
                        } else {
                            callback.onError(new Throwable("蓝牙打开失败"));
                        }
                    }
                });
            }
        });
    }

    public static void disable() {
        disable(null);
    }

    public static void disable(final Callback callback) {
        if (null == callback) {
            if (null != sAdapter) {
                sAdapter.disable();
            }
            return;
        }
        Blue.execute(new Runnable() {
            @Override
            public void run() {
                if (null == sAdapter) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(new Throwable("Bluetooth not supported"));
                        }
                    });
                    return;
                }
                sAdapter.disable();
                int count = 0;
                while (count++ < 5 && sAdapter.isEnabled()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (sAdapter.isEnabled()) {
                            callback.onResponse(Byte.MIN_VALUE, null);
                        } else {
                            callback.onError(new Throwable("蓝牙打开失败"));
                        }
                    }
                });
            }
        });
    }

    public static boolean isEnabled() {
        return null != sAdapter && sAdapter.isEnabled();
    }

    @IntDef({STATE_CONNECTED, STATE_CONNECTING, STATE_DISCONNECTED, STATE_DISCONNECTING, STATE_CONNECT_FAILURE, STATE_DISCONNECT_FAILURE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ConnectionState {

    }

    protected static final ExecutorService mExecutorService = Executors.newCachedThreadPool();

    protected static Handler handler = new Handler(Looper.getMainLooper());

    protected static BlueAdapter sAdapter = BlueAdapter.getDefaultAdapter();

    private static Map<String, Blue> blueMap = new HashMap<>();

    private final Object stateLock = new Object();

    private int mRetryTimes = 5;

    @ConnectionState
    private volatile int state = STATE_DISCONNECTED;

    public static void init(Application app) {
        sContext = app;
    }

    public void setRetryTimes(int retryTimes) {
        this.mRetryTimes = retryTimes;
    }

    public int getRetryTimes() {
        return mRetryTimes;
    }

    public static Blue get(BlueDevice device, String uuid) {
        return get(device, uuid, false);
    }

    public static Blue get(BlueDevice device, String uuid, boolean insecure) {
        if (null == sContext) {
            throw new RuntimeException("Blue is not inited");
        }
        String secondaryKey = device.getAddress() + "-" + uuid;
        if (!blueMap.containsKey(secondaryKey)) {
            Blue blue = new BlueIml(sContext, device, uuid, insecure);
            blueMap.put(secondaryKey, blue);
            return blue;
        }
        return blueMap.get(secondaryKey);
    }

    public abstract void setConnectionCallback(ConnectionCallback callback);

    public abstract void setGlobalCallback(Callback receiver);

    public abstract void setProcessor(Processor processor);

    public abstract void setFactory(Factory factory);

    public abstract void addFactory(Factory factory);

    public abstract void removeFactory(Factory factory);

    public abstract void connect();

    public abstract void disconnect();

    public abstract void send(byte[] cmd);

    public abstract void send(byte[] cmd, Callback callback);

    public abstract void send(byte[] cmd, boolean removeCallback, Callback callback);

    public abstract void send(String cmd);

    public abstract void send(String cmd, Callback callback);

    public abstract void send(String cmd, boolean removeCallback, Callback callback);

    public abstract BlueDevice getDevice();

    @ConnectionState
    public final int getState() {
        final int localState;
        synchronized (stateLock) {
            localState = state;
        }
        return localState;
    }

    protected synchronized final void setState(@ConnectionState int state) {
        setState(state, true);
    }

    protected synchronized final void setState(@ConnectionState int state, boolean notify) {
        int lastState = getState();
        if (lastState == state) {
            return;
        }
        onStateChanging(lastState, state, notify);
        synchronized (stateLock) {
            this.state = state;
        }
        System.out.println("onStateChanged current state=" + getState());
        onStateChanged(lastState, getState(), notify);
    }

    protected void onStateChanging(int lastState, int nowState, boolean notify) {
    }

    protected void onStateChanged(int lastState, int state, boolean notify) {
    }

    private static BroadcastReceiver mDiscoveryReceiver = null;

    public static void startDiscovery(final DiscoveryCallback callback) {
        if (null == sAdapter) {
            Toast.makeText(sContext.getApplicationContext(), "当前设备不支持蓝牙", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!sAdapter.isEnabled()) {
            Toast.makeText(sContext.getApplicationContext(), "请打开蓝牙", Toast.LENGTH_SHORT).show();
            return;
        }
        final DiscoveryCallback wrapper = new DiscoveryCallback() {
            @Override
            public void onDiscoveryStarted() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onDiscoveryStarted();
                    }
                });
            }

            @Override
            public void onDiscoveryFinished() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onDiscoveryFinished();
                    }
                });
            }

            @Override
            public void onDeviceFound(final BlueDevice device) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onDeviceFound(device);
                    }
                });
            }
        };
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                unregisterDiscoveryReceiver();
                while (sAdapter.isDiscovering()) ;
                registerDiscoveryReceiver(wrapper);
            }
        });
    }

    public static void cancelDiscovery() {
        if (null == sContext) {
            return;
        }
        if (null == sAdapter) {
            Toast.makeText(sContext.getApplicationContext(), "当前设备不支持蓝牙", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!sAdapter.isEnabled()) {
            return;
        }
        unregisterDiscoveryReceiver();
    }

    private static void registerDiscoveryReceiver(final DiscoveryCallback callback) {
        unregisterDiscoveryReceiver();

        mDiscoveryReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (null == intent) {
                    return;
                }
                if (BlueAdapter.ACTION_DISCOVERY_STARTED.equals(intent.getAction())) {
                    callback.onDiscoveryStarted();
                } else if (BlueDevice.ACTION_FOUND.equals(intent.getAction())) {
                    BlueDevice device = BlueDevice.fromIntent(intent);
                    device.setData(intent.getExtras());
                    callback.onDeviceFound(device);
                } else if (BlueAdapter.ACTION_DISCOVERY_FINISHED.equals(intent.getAction())) {
                    unregisterDiscoveryReceiver();
                    callback.onDiscoveryFinished();
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(BlueAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BlueAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BlueDevice.ACTION_FOUND);
        sContext.registerReceiver(mDiscoveryReceiver, filter);
        if (sAdapter.isDiscovering()) {
            sAdapter.cancelDiscovery();
        }
        sAdapter.startDiscovery();
    }

    private static void unregisterDiscoveryReceiver() {
        if (null != mDiscoveryReceiver) {
            try {
                sContext.getApplicationContext().unregisterReceiver(mDiscoveryReceiver);
                mDiscoveryReceiver = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (sAdapter.isDiscovering()) {
            sAdapter.cancelDiscovery();
        }
    }

    public interface DiscoveryCallback {

        void onDiscoveryStarted();

        void onDiscoveryFinished();

        void onDeviceFound(BlueDevice device);
    }
}