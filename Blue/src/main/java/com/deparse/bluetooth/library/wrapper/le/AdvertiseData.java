package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class AdvertiseData {
    private final android.bluetooth.le.AdvertiseData mOrigin;

    public AdvertiseData(android.bluetooth.le.AdvertiseData origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    public boolean equals(Object arg0) {
        return mOrigin.equals(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean getIncludeDeviceName() {
        return mOrigin.getIncludeDeviceName();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean getIncludeTxPowerLevel() {
        return mOrigin.getIncludeTxPowerLevel();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.util.SparseArray getManufacturerSpecificData() {
        return mOrigin.getManufacturerSpecificData();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public java.util.Map getServiceData() {
        return mOrigin.getServiceData();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public java.util.List getServiceUuids() {
        return mOrigin.getServiceUuids();
    }


    public int hashCode() {
        return mOrigin.hashCode();
    }


    public String toString() {
        return mOrigin.toString();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
