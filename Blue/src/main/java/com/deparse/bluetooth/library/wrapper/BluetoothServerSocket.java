package com.deparse.bluetooth.library.wrapper;

public class BluetoothServerSocket {
    private final android.bluetooth.BluetoothServerSocket mOrigin;

    public BluetoothServerSocket(android.bluetooth.BluetoothServerSocket origin) {
        this.mOrigin = origin;
    }

    public android.bluetooth.BluetoothSocket accept() throws java.io.IOException {
        return mOrigin.accept();
    }


    public android.bluetooth.BluetoothSocket accept(int arg0) throws java.io.IOException {
        return mOrigin.accept(arg0);
    }


    public void close() throws java.io.IOException {
        mOrigin.close();
    }

    public String toString() {
        return mOrigin.toString();
    }
}
