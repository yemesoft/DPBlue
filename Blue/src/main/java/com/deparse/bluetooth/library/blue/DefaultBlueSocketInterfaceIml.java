package com.deparse.bluetooth.library.blue;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DefaultBlueSocketInterfaceIml implements BlueSocketInterface {
    private final BluetoothSocket mBluetoothSocket;

    public DefaultBlueSocketInterfaceIml(BluetoothSocket socket) throws IOException {
        mBluetoothSocket = socket;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return mBluetoothSocket.getInputStream();
    }

    @Override
    public OutputStream OutputStream() throws IOException {
        return mBluetoothSocket.getOutputStream();
    }

    @Override
    public void close() throws IOException {
        mBluetoothSocket.close();
    }

    @Override
    public boolean isConnected() {
        return mBluetoothSocket.isConnected();
    }

    @Override
    public void connect() throws IOException {
        mBluetoothSocket.connect();
    }
}
