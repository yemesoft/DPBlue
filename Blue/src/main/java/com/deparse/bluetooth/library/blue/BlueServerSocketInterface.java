package com.deparse.bluetooth.library.blue;

import java.io.IOException;

public interface BlueServerSocketInterface {
    BlueSocket accept() throws IOException;

    void close() throws IOException;
}
