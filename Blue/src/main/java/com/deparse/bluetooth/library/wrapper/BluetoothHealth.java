package com.deparse.bluetooth.library.wrapper;

public class BluetoothHealth {
    private final android.bluetooth.BluetoothHealth mOrigin;

    public BluetoothHealth(android.bluetooth.BluetoothHealth origin) {
        this.mOrigin = origin;
    }


    public boolean connectChannelToSource(android.bluetooth.BluetoothDevice arg0, android.bluetooth.BluetoothHealthAppConfiguration arg1) {
        return mOrigin.connectChannelToSource(arg0, arg1);
    }


    public boolean disconnectChannel(android.bluetooth.BluetoothDevice arg0, android.bluetooth.BluetoothHealthAppConfiguration arg1, int arg2) {
        return mOrigin.disconnectChannel(arg0, arg1, arg2);
    }


    public java.util.List getConnectedDevices() {
        return mOrigin.getConnectedDevices();
    }


    public int getConnectionState(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.getConnectionState(arg0);
    }


    public java.util.List getDevicesMatchingConnectionStates(int[] arg0) {
        return mOrigin.getDevicesMatchingConnectionStates(arg0);
    }


    public android.os.ParcelFileDescriptor getMainChannelFd(android.bluetooth.BluetoothDevice arg0, android.bluetooth.BluetoothHealthAppConfiguration arg1) {
        return mOrigin.getMainChannelFd(arg0, arg1);
    }


    public boolean registerSinkAppConfiguration(String arg0, int arg1, android.bluetooth.BluetoothHealthCallback arg2) {
        return mOrigin.registerSinkAppConfiguration(arg0, arg1, arg2);
    }


    public boolean unregisterAppConfiguration(android.bluetooth.BluetoothHealthAppConfiguration arg0) {
        return mOrigin.unregisterAppConfiguration(arg0);
    }
}
