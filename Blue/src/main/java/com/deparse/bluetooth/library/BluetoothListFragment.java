package com.deparse.bluetooth.library;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.deparse.bluetooth.library.blue.BlueDevice;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BluetoothListFragment extends Fragment {
    private ListView listView;
    private DeviceListAdapter adapter;
    private Blue.DiscoveryCallback mDiscoveryCallback = null;
    private OnDeviceSelectedListener mOnDeviceSelectedListener;

    private Blue.DiscoveryCallback mInnerDiscoveryCallback = new Blue.DiscoveryCallback() {
        @Override
        public void onDiscoveryStarted() {
            if (null != mDiscoveryCallback) {
                mDiscoveryCallback.onDiscoveryStarted();
            }
        }

        @Override
        public void onDiscoveryFinished() {
            if (null != mDiscoveryCallback) {
                mDiscoveryCallback.onDiscoveryFinished();
            }
        }

        @Override
        public void onDeviceFound(BlueDevice device) {
            if (null != mDiscoveryCallback) {
                mDiscoveryCallback.onDeviceFound(device);
            }
            adapter.add(device);
        }
    };

    public void setOnDeviceSelectedListener(OnDeviceSelectedListener listener) {
        this.mOnDeviceSelectedListener = listener;
    }

    public void refresh() {
        requestPermissions(new String[]{
                Manifest.permission.BLUETOOTH
                , Manifest.permission.BLUETOOTH_ADMIN
                , Manifest.permission.ACCESS_COARSE_LOCATION
                , Manifest.permission.ACCESS_FINE_LOCATION
        }, 0x8001);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (null == getActivity() || 0x8001 != requestCode) {
            return;
        }
        boolean granted = true;
        for (int r : grantResults) {
            if (r != PackageManager.PERMISSION_GRANTED) {
                granted = false;
                break;
            }
        }
        if (!granted) {
            Toast.makeText(getActivity().getApplicationContext(), "权限申请失败", Toast.LENGTH_SHORT).show();
            return;
        }
        adapter.clear();
        Blue.startDiscovery(mInnerDiscoveryCallback);
    }

    public interface OnDeviceSelectedListener {

        void onDeviceSelected(BlueDevice device);
    }

    public void setDiscoveryCallback(Blue.DiscoveryCallback callback) {
        this.mDiscoveryCallback = callback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listView = new ListView(getActivity());
        adapter = new DeviceListAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (null != mOnDeviceSelectedListener) {
                    mOnDeviceSelectedListener.onDeviceSelected(adapter.getDevice(position));
                }
            }
        });
        refresh();
        return listView;
    }

    @Override
    public void onDestroyView() {
        Blue.cancelDiscovery();
        super.onDestroyView();
    }

    private class DeviceListAdapter extends BaseAdapter {

        private final List<BlueDevice> deviceList = new ArrayList<BlueDevice>() {
            @Override
            public boolean remove(@Nullable Object o) {
                if (!(o instanceof BlueDevice)) {
                    return false;
                }
                for (BlueDevice device : this) {
                    if (null != device.getAddress() && device.getAddress().equals(((BlueDevice) o).getAddress())) {
                        return super.remove(device);
                    }
                }
                return false;
            }
        };

        @Override
        public int getCount() {
            return deviceList.size();
        }

        @Override
        public Object getItem(int position) {
            return deviceList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public BlueDevice getDevice(int position) {
            return deviceList.get(position);
        }

        public void add(BlueDevice device) {
            deviceList.remove(device);
            deviceList.add(device);
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView;
            if (null == convertView) {
                textView = new TextView(getActivity());
            } else {
                textView = (TextView) convertView;
            }
            BlueDevice device = getDevice(position);
            if (null != device) {
                textView.setText(TextUtils.isEmpty(device.getName()) ? device.getAddress() : device.getName());
                textView.setPadding(0, 30, 0, 30);
            }
            return textView;
        }

        public void clear() {
            deviceList.clear();
            notifyDataSetChanged();
        }
    }
}
