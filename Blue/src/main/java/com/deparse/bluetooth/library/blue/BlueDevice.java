package com.deparse.bluetooth.library.blue;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BlueDevice implements BlueDeviceInterface {
    public static final String ACTION_FOUND = BluetoothDevice.ACTION_FOUND;
    public static final String EXTRA_DEVICE = BluetoothDevice.EXTRA_DEVICE;
    public static final String ACTION_BOND_STATE_CHANGED = BluetoothDevice.ACTION_BOND_STATE_CHANGED;
    public static final String ACTION_ACL_DISCONNECTED = BluetoothDevice.ACTION_ACL_DISCONNECTED;
    public static final String EXTRA_BOND_STATE = BluetoothDevice.EXTRA_BOND_STATE;
    public static final int BOND_NONE = BluetoothDevice.BOND_NONE;
    public static final int BOND_BONDED = BluetoothDevice.BOND_BONDED;

    private static final Map<String, BlueDevice> deviceMap = new HashMap<>();

    private final BlueDeviceInterface mBlueDeviceInterface;
    private Bundle mExtras;

    public BlueDevice() {
        this.mBlueDeviceInterface = createDefaultBlueDeviceInterface();
    }

    public BlueDevice(@NonNull BlueDeviceInterface anInterface) {
        Objects.requireNonNull(anInterface);
        this.mBlueDeviceInterface = anInterface;
    }

    public static BlueDevice create(BluetoothDevice device) {
        if (deviceMap.containsKey(device.getAddress())) {
            return deviceMap.get(device.getAddress());
        }
        return create(new DefaultBlueDeviceInterfaceIml(device));
    }

    public static BlueDevice create(BlueDeviceInterface anInterface) {
        if (deviceMap.containsKey(anInterface.getAddress())) {
            return deviceMap.get(anInterface.getAddress());
        }
        BlueDevice d = new BlueDevice(anInterface);
        deviceMap.put(anInterface.getAddress(), d);
        return d;
    }

    public static BlueDevice fromIntent(Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BlueDevice.EXTRA_DEVICE);
        return create(device);
    }

    @Override
    public final String getAddress() {
        return mBlueDeviceInterface.getAddress();
    }

    @Override
    public final boolean createBond() {
        return mBlueDeviceInterface.createBond();
    }

    @Override
    public final boolean removeBond() {
        return mBlueDeviceInterface.removeBond();
    }

    @Override
    public final int getBondState() {
        return mBlueDeviceInterface.getBondState();
    }

    @Override
    public final BlueSocket createInsecureRfcommSocketToServiceRecord(UUID uuid) throws IOException {
        return mBlueDeviceInterface.createInsecureRfcommSocketToServiceRecord(uuid);
    }

    @Override
    public final BlueSocket createRfcommSocketToServiceRecord(UUID uuid) throws IOException {
        return mBlueDeviceInterface.createRfcommSocketToServiceRecord(uuid);
    }

    @Override
    public final String getName() {
        return mBlueDeviceInterface.getName();
    }

    @Override
    public final boolean equals(@Nullable Object obj) {
        if (!(obj instanceof BlueDeviceInterface)) {
            return false;
        }
        return getAddress().equals(((BlueDeviceInterface) obj).getAddress());
    }

    @NonNull
    @Override
    public final String toString() {
        return null == mBlueDeviceInterface ? getClass().getSimpleName() : mBlueDeviceInterface.toString();
    }

    public final void setData(Bundle extras) {
        this.mExtras = extras;
    }

    public final Bundle getExtras() {
        return mExtras;
    }

    protected BlueDeviceInterface createDefaultBlueDeviceInterface() {
        throw new RuntimeException("createDefaultBlueDeviceInterface not overrode");
    }

    @Override
    public BluetoothDevice getRealDevice() {
        return mBlueDeviceInterface.getRealDevice();
    }
}