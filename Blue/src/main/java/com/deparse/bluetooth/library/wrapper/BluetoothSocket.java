package com.deparse.bluetooth.library.wrapper;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class BluetoothSocket {
    private final android.bluetooth.BluetoothSocket mOrigin;

    public BluetoothSocket(android.bluetooth.BluetoothSocket origin) {
        this.mOrigin = origin;
    }

    public void close() throws java.io.IOException {
        mOrigin.close();
    }


    public void connect() throws java.io.IOException {
        mOrigin.connect();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public int getConnectionType() {
        return mOrigin.getConnectionType();
    }


    public java.io.InputStream getInputStream() throws java.io.IOException {
        return mOrigin.getInputStream();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public int getMaxReceivePacketSize() {
        return mOrigin.getMaxReceivePacketSize();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public int getMaxTransmitPacketSize() {
        return mOrigin.getMaxTransmitPacketSize();
    }


    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        return mOrigin.getOutputStream();
    }


    public BluetoothDevice getRemoteDevice() {
        return new BluetoothDevice(mOrigin.getRemoteDevice());
    }


    public boolean isConnected() {
        return mOrigin.isConnected();
    }
}
