package com.deparse.bluetooth.library.wrapper;

public class BluetoothGattCharacteristic {
    private final android.bluetooth.BluetoothGattCharacteristic mOrigin;

    public BluetoothGattCharacteristic(android.bluetooth.BluetoothGattCharacteristic origin) {
        this.mOrigin = origin;
    }

    public boolean addDescriptor(android.bluetooth.BluetoothGattDescriptor arg0) {
        return mOrigin.addDescriptor(arg0);
    }


    public int describeContents() {
        return mOrigin.describeContents();
    }


    public android.bluetooth.BluetoothGattDescriptor getDescriptor(java.util.UUID arg0) {
        return mOrigin.getDescriptor(arg0);
    }


    public java.util.List getDescriptors() {
        return mOrigin.getDescriptors();
    }


    public Float getFloatValue(int arg0, int arg1) {
        return mOrigin.getFloatValue(arg0, arg1);
    }


    public int getInstanceId() {
        return mOrigin.getInstanceId();
    }


    public Integer getIntValue(int arg0, int arg1) {
        return mOrigin.getIntValue(arg0, arg1);
    }


    public int getPermissions() {
        return mOrigin.getPermissions();
    }


    public int getProperties() {
        return mOrigin.getProperties();
    }


    public android.bluetooth.BluetoothGattService getService() {
        return mOrigin.getService();
    }


    public String getStringValue(int arg0) {
        return mOrigin.getStringValue(arg0);
    }


    public java.util.UUID getUuid() {
        return mOrigin.getUuid();
    }


    public byte[] getValue() {
        return mOrigin.getValue();
    }


    public int getWriteType() {
        return mOrigin.getWriteType();
    }


    public boolean setValue(String arg0) {
        return mOrigin.setValue(arg0);
    }


    public boolean setValue(byte[] arg0) {
        return mOrigin.setValue(arg0);
    }


    public boolean setValue(int arg0, int arg1, int arg2) {
        return mOrigin.setValue(arg0, arg1, arg2);
    }


    public boolean setValue(int arg0, int arg1, int arg2, int arg3) {
        return mOrigin.setValue(arg0, arg1, arg2, arg3);
    }


    public void setWriteType(int arg0) {
        mOrigin.setWriteType(arg0);
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
