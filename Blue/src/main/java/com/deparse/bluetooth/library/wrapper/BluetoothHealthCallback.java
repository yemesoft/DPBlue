package com.deparse.bluetooth.library.wrapper;

public class BluetoothHealthCallback {
    private final android.bluetooth.BluetoothHealthCallback mOrigin;

    public BluetoothHealthCallback(android.bluetooth.BluetoothHealthCallback origin) {
        this.mOrigin = origin;
    }

    public void onHealthAppConfigurationStatusChange(android.bluetooth.BluetoothHealthAppConfiguration arg0, int arg1) {
        mOrigin.onHealthAppConfigurationStatusChange(arg0, arg1);
    }


    public void onHealthChannelStateChange(android.bluetooth.BluetoothHealthAppConfiguration arg0, android.bluetooth.BluetoothDevice arg1, int arg2, int arg3, android.os.ParcelFileDescriptor arg4, int arg5) {
        mOrigin.onHealthChannelStateChange(arg0, arg1, arg2, arg3, arg4, arg5);
    }
}
