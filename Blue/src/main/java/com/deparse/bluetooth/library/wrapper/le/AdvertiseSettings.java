package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class AdvertiseSettings {
    private final android.bluetooth.le.AdvertiseSettings mOrigin;

    public AdvertiseSettings(android.bluetooth.le.AdvertiseSettings origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getMode() {
        return mOrigin.getMode();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getTimeout() {
        return mOrigin.getTimeout();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getTxPowerLevel() {
        return mOrigin.getTxPowerLevel();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean isConnectable() {
        return mOrigin.isConnectable();
    }


    public String toString() {
        return mOrigin.toString();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
