package com.deparse.bluetooth.library.wrapper;

public class BluetoothGattService {
    private final android.bluetooth.BluetoothGattService mOrigin;

    public BluetoothGattService(android.bluetooth.BluetoothGattService origin) {
        this.mOrigin = origin;
    }

    public boolean addCharacteristic(android.bluetooth.BluetoothGattCharacteristic arg0) {
        return mOrigin.addCharacteristic(arg0);
    }

    public boolean addService(android.bluetooth.BluetoothGattService arg0) {
        return mOrigin.addService(arg0);
    }


    public int describeContents() {
        return mOrigin.describeContents();
    }


    public android.bluetooth.BluetoothGattCharacteristic getCharacteristic(java.util.UUID arg0) {
        return mOrigin.getCharacteristic(arg0);
    }


    public java.util.List getCharacteristics() {
        return mOrigin.getCharacteristics();
    }


    public java.util.List getIncludedServices() {
        return mOrigin.getIncludedServices();
    }


    public int getInstanceId() {
        return mOrigin.getInstanceId();
    }


    public int getType() {
        return mOrigin.getType();
    }


    public java.util.UUID getUuid() {
        return mOrigin.getUuid();
    }

    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
