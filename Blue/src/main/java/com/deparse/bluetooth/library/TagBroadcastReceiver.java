package com.deparse.bluetooth.library;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("all")
abstract class TagBroadcastReceiver {
    private boolean registered;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TagBroadcastReceiver.this.onReceive(context, intent);
        }
    };
    private Object tag;

    protected abstract void onReceive(Context context, Intent intent);

    private boolean isRegistered() {
        return registered;
    }

    private void setRegistered(boolean registered) {
        this.registered = registered;
    }

    protected String[] getInnerActions() {
        return null;
    }

    public void register(Context context, String... actions) {
        context = context.getApplicationContext();
        if (isRegistered()) {
            unregister(context);
        }
        List<String> actionList = new ArrayList<>();
        String[] innerActions = getInnerActions();
        if (null != innerActions) {
            Collections.addAll(actionList, innerActions);
        }
        Collections.addAll(actionList, actions);
        IntentFilter filter = new IntentFilter();
        for (String action : actionList) {
            filter.addAction(action);
        }
        context.registerReceiver(mBroadcastReceiver, filter);
        setRegistered(true);
    }

    public void unregister(Context context) {
        if (!isRegistered()) {
            return;
        }
        context = context.getApplicationContext();
        context.unregisterReceiver(mBroadcastReceiver);
        setRegistered(false);
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public <T> T getTag() {
        return (T) tag;
    }
}