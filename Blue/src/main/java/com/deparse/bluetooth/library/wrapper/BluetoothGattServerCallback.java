package com.deparse.bluetooth.library.wrapper;

public class BluetoothGattServerCallback {
    private final android.bluetooth.BluetoothGattServerCallback mOrigin;

    public BluetoothGattServerCallback(android.bluetooth.BluetoothGattServerCallback origin) {
        this.mOrigin = origin;
    }

    public void onCharacteristicReadRequest(android.bluetooth.BluetoothDevice arg0, int arg1, int arg2, android.bluetooth.BluetoothGattCharacteristic arg3) {
        mOrigin.onCharacteristicReadRequest(arg0, arg1, arg2, arg3);
    }


    public void onCharacteristicWriteRequest(android.bluetooth.BluetoothDevice arg0, int arg1, android.bluetooth.BluetoothGattCharacteristic arg2, boolean arg3, boolean arg4, int arg5, byte[] arg6) {
        mOrigin.onCharacteristicWriteRequest(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }


    public void onConnectionStateChange(android.bluetooth.BluetoothDevice arg0, int arg1, int arg2) {
        mOrigin.onConnectionStateChange(arg0, arg1, arg2);
    }


    public void onDescriptorReadRequest(android.bluetooth.BluetoothDevice arg0, int arg1, int arg2, android.bluetooth.BluetoothGattDescriptor arg3) {
        mOrigin.onDescriptorReadRequest(arg0, arg1, arg2, arg3);
    }


    public void onDescriptorWriteRequest(android.bluetooth.BluetoothDevice arg0, int arg1, android.bluetooth.BluetoothGattDescriptor arg2, boolean arg3, boolean arg4, int arg5, byte[] arg6) {
        mOrigin.onDescriptorWriteRequest(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }


    public void onExecuteWrite(android.bluetooth.BluetoothDevice arg0, int arg1, boolean arg2) {
        mOrigin.onExecuteWrite(arg0, arg1, arg2);
    }


    public void onServiceAdded(int arg0, android.bluetooth.BluetoothGattService arg1) {
        mOrigin.onServiceAdded(arg0, arg1);
    }
}
