package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class ScanSettings {
    private final android.bluetooth.le.ScanSettings mOrigin;

    public ScanSettings(android.bluetooth.le.ScanSettings origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getCallbackType() {
        return mOrigin.getCallbackType();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean getLegacy() {
        return mOrigin.getLegacy();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getPhy() {
        return mOrigin.getPhy();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public long getReportDelayMillis() {
        return mOrigin.getReportDelayMillis();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getScanMode() {
        return mOrigin.getScanMode();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getScanResultType() {
        return mOrigin.getScanResultType();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
