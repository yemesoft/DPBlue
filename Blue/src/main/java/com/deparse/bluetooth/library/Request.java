package com.deparse.bluetooth.library;

import androidx.annotation.NonNull;

public class Request {
    public byte[] cmd;
    public Callback callback;
    public boolean removeCallback;

    Request(byte[] cmd, boolean removeCallback, Callback callback) {
        this.cmd = cmd;
        this.callback = callback;
        this.removeCallback = removeCallback;
    }

    @NonNull
    @Override
    public String toString() {
        return "cmd=" + BlueUtil.bytesToHex(cmd) + ", removeCallback=" + removeCallback + ", callback=" + callback;
    }
}
