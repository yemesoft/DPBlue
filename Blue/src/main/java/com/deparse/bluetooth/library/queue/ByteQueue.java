package com.deparse.bluetooth.library.queue;

public interface ByteQueue {
    void put(byte[] bytes, int offset, int length) throws InterruptedException;

    void take(byte[] out, int offset, int length) throws InterruptedException;

    void at(int offset);

    void skip(int offset);
}
