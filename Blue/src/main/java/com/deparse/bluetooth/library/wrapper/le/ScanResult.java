package com.deparse.bluetooth.library.wrapper.le;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class ScanResult {
    private final android.bluetooth.le.ScanResult mOrigin;

    public ScanResult(android.bluetooth.le.ScanResult origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    public boolean equals(Object arg0) {
        return mOrigin.equals(arg0);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getAdvertisingSid() {
        return mOrigin.getAdvertisingSid();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getDataStatus() {
        return mOrigin.getDataStatus();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.bluetooth.BluetoothDevice getDevice() {
        return mOrigin.getDevice();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getPeriodicAdvertisingInterval() {
        return mOrigin.getPeriodicAdvertisingInterval();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getPrimaryPhy() {
        return mOrigin.getPrimaryPhy();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getRssi() {
        return mOrigin.getRssi();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public android.bluetooth.le.ScanRecord getScanRecord() {
        return mOrigin.getScanRecord();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getSecondaryPhy() {
        return mOrigin.getSecondaryPhy();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public long getTimestampNanos() {
        return mOrigin.getTimestampNanos();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public int getTxPower() {
        return mOrigin.getTxPower();
    }


    public int hashCode() {
        return mOrigin.hashCode();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isConnectable() {
        return mOrigin.isConnectable();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isLegacy() {
        return mOrigin.isLegacy();
    }


    public String toString() {
        return mOrigin.toString();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
