package com.deparse.bluetooth.library;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

import dalvik.system.DexClassLoader;

public class DefaultSimulator extends Simulator {

    private final Config mConfig;

    private CyclerByteQueue mQueue = new CyclerByteQueue(1024);

    private final WeakHashMap<ClientSocket, ResponseSender> senders = new WeakHashMap<>();

    public DefaultSimulator(Config config) {
        this.mConfig = config;
    }

    private int max(int... args) {
        int max = args[0];
        for (int arg : args) {
            max = Math.max(max, arg);
        }
        return max;
    }

    @Override
    protected void onReceived(ClientSocket socket, byte[] data) {
        ResponseSender sender = senders.get(socket);
        if (null == sender) {
            sender = new ResponseSender();
            senders.put(socket, sender);
        }
        DexClassLoader loader = new DexClassLoader("", "", "", ClassLoader.getSystemClassLoader());
        try {
            final byte[] startBytes = mConfig.startBytes;
            final byte[] endBytes = mConfig.endBytes;
            if (null == startBytes || null == endBytes) {
                socket.write(data);
                socket.flush();
                return;
            }
            mQueue.put(data);
            int maxPre = max(mConfig.flagEnd, mConfig.lengthEnd);
            int extraLength = maxPre + endBytes.length + 1;
            while (mQueue.size() >= extraLength) {
                while (mQueue.size() >= extraLength && !byteEquals(startBytes, mQueue, 0, startBytes.length - 1)) {
                    mQueue.get();
                }
                if (mQueue.size() < extraLength) {
                    return;
                }
                int length = 0;
                for (int i = 0, count = mConfig.lengthEnd - mConfig.lengthStart + 1; i < count; i++) {
                    length |= mQueue.at(i + mConfig.lengthStart) << ((count - i - 1) * 8);
                }
                if (!byteEquals(endBytes, mQueue, maxPre, maxPre + endBytes.length - 1)) {
                    mQueue.get();
                    continue;
                }
                byte[] packet = new byte[extraLength + length];
                mQueue.get(packet);
                String key = BlueUtil.bytesToHex(packet);
                Response response = mConfig.get(key);
                if (null != response) {
                    sender.send(socket, response);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean byteEquals(byte[] one, CyclerByteQueue queue, int from, int to) {
        if (null == one || to - from + 1 > queue.size()) {
            return false;
        }
        for (int i = 0, count = to - from + 1; i < count; i++) {
            if (one[i] != queue.at(i + from)) {
                return false;
            }
        }
        return true;
    }

    public static class Config {
        private final String start;
        private final String end;
        private final String flagRange;
        private final String lengthRange;

        private final byte[] startBytes;
        private final byte[] endBytes;
        private final int flagStart;
        private final int flagEnd;
        private final int lengthStart;
        private final int lengthEnd;

        private final Map<String, Response> mapping = new HashMap<>();

        private static int[] parseRange(String range) {
            if (null == range) {
                return new int[]{-1, -1};
            }
            int[] result = new int[2];
            if (range.contains(",")) {
                String[] arr = range.split(",");
                result[0] = Integer.valueOf(arr[0]);
                result[1] = Integer.valueOf(arr[1]);
            } else {
                int i = Integer.valueOf(range);
                result[0] = i;
                result[1] = i;
            }
            return result;
        }

        public Config(String configJson) throws JSONException {
            JSONObject obj = new JSONObject(configJson);
            start = obj.optString("start", null);
            startBytes = null == start ? null : BlueUtil.hexToBytes(start);

            end = obj.optString("end", null);
            endBytes = null == end ? null : BlueUtil.hexToBytes(end);

            flagRange = obj.optString("flagRange", null);
            int[] tempFlag = parseRange(flagRange);
            flagStart = tempFlag[0];
            flagEnd = tempFlag[1];

            lengthRange = obj.optString("lengthRange", null);
            int[] tempLength = parseRange(lengthRange);
            lengthStart = tempLength[0];
            lengthEnd = tempLength[1];

            JSONObject localMapping = obj.optJSONObject("mapping");
            if (null != localMapping) {
                Iterator<String> keys = localMapping.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    JSONObject value = localMapping.optJSONObject(key);
                    key = key.replaceAll("^0[x|X]", "").toLowerCase();
                    if (null != value) {
                        Response response = new Response();
                        response.type = value.optString("type");
                        response.duration = value.optLong("duration", 0);
                        JSONArray arr = value.optJSONArray("data");
                        if (null != arr) {
                            response.data = new String[arr.length()];
                            for (int i = 0; i < arr.length(); i++) {
                                response.data[i] = arr.optString(i, null);
                            }
                        }
                        localMapping.put(key, response);
                    }
                }
            }
        }

        public Response get(String cmd) {
            return mapping.get(cmd);
        }
    }

    public static class Response {
        private String type;
        private long duration;
        private String[] data;

        public byte[] getByteData(int index) {
            return BlueUtil.hexToBytes(data[index]);
        }
    }

    private static class ResponseSender extends Thread implements Handler.Callback {
        Handler handler = null;
        private static final Object lock = new Object();
        private boolean isLooping = false, isStopped = true;

        @Override
        public void run() {
            Looper.prepare();
            handler = new Handler(Looper.myLooper(), this);
            Looper.loop();
        }

        @Override
        public boolean handleMessage(Message msg) {
            try {
                Object[] objects = (Object[]) msg.obj;
                ClientSocket socket = (ClientSocket) objects[0];
                Response response = (Response) objects[1];
                if ("oneshot".equals(response.type)) {
                    if (null != response.data) {
                        for (int i = 0; i < response.data.length; i++) {
                            byte[] bytes = response.getByteData(i);
                            socket.write(bytes);
                            socket.flush();
                            if (response.duration > 0) {
                                Thread.sleep(response.duration);
                            }
                        }
                    }
                } else if ("loop".equals(response.type)) {
                    if (!isStopped) {
                        synchronized (lock) {
                            if (isStopped && isLooping) {
                                isLooping = false;
                            }
                        }
                    }
                    synchronized (lock) {
                        if (isLooping) {
                            isLooping = false;
                        }
                    }
                    new Thread() {
                        @Override
                        public void run() {

                        }
                    }.start();
                } else if ("break-loop".equals(response.type)) {

                }
            } catch (Exception e) {

            }
            return false;
        }

        void send(ClientSocket socket, Response response) throws IOException, InterruptedException {
            if (null != response.data) {
                Message msg = Message.obtain();
                msg.obj = new Object[]{socket, response};
                handler.sendMessage(msg);
            }
        }
    }
}
