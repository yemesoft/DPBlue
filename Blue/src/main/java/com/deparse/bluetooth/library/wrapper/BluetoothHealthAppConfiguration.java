package com.deparse.bluetooth.library.wrapper;

public class BluetoothHealthAppConfiguration {
    private final android.bluetooth.BluetoothHealthAppConfiguration mOrigin;

    public BluetoothHealthAppConfiguration(android.bluetooth.BluetoothHealthAppConfiguration origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    public boolean equals(Object arg0) {
        return mOrigin.equals(arg0);
    }


    public int getDataType() {
        return mOrigin.getDataType();
    }


    public String getName() {
        return mOrigin.getName();
    }


    public int getRole() {
        return mOrigin.getRole();
    }


    public int hashCode() {
        return mOrigin.hashCode();
    }


    public String toString() {
        return mOrigin.toString();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
