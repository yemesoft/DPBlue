package com.deparse.bluetooth.library.wrapper;

public class BluetoothClass {
    private final android.bluetooth.BluetoothClass mOrigin;

    public BluetoothClass(android.bluetooth.BluetoothClass origin) {
        this.mOrigin = origin;
    }

    public int describeContents() {
        return mOrigin.describeContents();
    }


    public boolean equals(Object arg0) {
        return mOrigin.equals(arg0);
    }


    public int getDeviceClass() {
        return mOrigin.getDeviceClass();
    }


    public int getMajorDeviceClass() {
        return mOrigin.getMajorDeviceClass();
    }


    public boolean hasService(int arg0) {
        return mOrigin.hasService(arg0);
    }


    public int hashCode() {
        return mOrigin.hashCode();
    }


    public String toString() {
        return mOrigin.toString();
    }


    public void writeToParcel(android.os.Parcel arg0, int arg1) {
        mOrigin.writeToParcel(arg0, arg1);
    }
}
