package com.deparse.bluetooth.library.wrapper;

public class BluetoothAssignedNumbers {
    private final android.bluetooth.BluetoothAssignedNumbers mOrigin;

    public BluetoothAssignedNumbers(android.bluetooth.BluetoothAssignedNumbers origin) {
        this.mOrigin = origin;
    }
}
