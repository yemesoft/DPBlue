package com.deparse.bluetooth.library.wrapper;

public class BluetoothA2dp {
    private final android.bluetooth.BluetoothA2dp mOrigin;

    public BluetoothA2dp(android.bluetooth.BluetoothA2dp origin) {
        this.mOrigin = origin;
    }


    public void finalize() {
        mOrigin.finalize();
    }


    public java.util.List getConnectedDevices() {
        return mOrigin.getConnectedDevices();
    }


    public int getConnectionState(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.getConnectionState(arg0);
    }


    public java.util.List getDevicesMatchingConnectionStates(int[] arg0) {
        return mOrigin.getDevicesMatchingConnectionStates(arg0);
    }


    public boolean isA2dpPlaying(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.isA2dpPlaying(arg0);
    }
}
