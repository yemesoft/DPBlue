package com.deparse.bluetooth.library.wrapper;

public class BluetoothHeadset {
    private final android.bluetooth.BluetoothHeadset mOrigin;

    public BluetoothHeadset(android.bluetooth.BluetoothHeadset origin) {
        this.mOrigin = origin;
    }

    public java.util.List getConnectedDevices() {
        return mOrigin.getConnectedDevices();
    }


    public int getConnectionState(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.getConnectionState(arg0);
    }


    public java.util.List getDevicesMatchingConnectionStates(int[] arg0) {
        return mOrigin.getDevicesMatchingConnectionStates(arg0);
    }

    public boolean isAudioConnected(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.isAudioConnected(arg0);
    }


    public boolean sendVendorSpecificResultCode(android.bluetooth.BluetoothDevice arg0, String arg1, String arg2) {
        return mOrigin.sendVendorSpecificResultCode(arg0, arg1, arg2);
    }


    public boolean startVoiceRecognition(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.startVoiceRecognition(arg0);
    }


    public boolean stopVoiceRecognition(android.bluetooth.BluetoothDevice arg0) {
        return mOrigin.stopVoiceRecognition(arg0);
    }
}
