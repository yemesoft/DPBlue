package com.deparse.bluetester;

import android.Manifest;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAssignedNumbers;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothHealth;
import android.bluetooth.BluetoothHealthAppConfiguration;
import android.bluetooth.BluetoothHealthCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Locale;
import java.util.zip.ZipFile;

@SuppressWarnings("all")
public class MainActivity extends AppCompatActivity {
    private Class[] classes = {
            AdvertiseData.class,
            AdvertiseSettings.class,
            BluetoothLeAdvertiser.class,
            BluetoothLeScanner.class,
            ScanCallback.class,
            ScanFilter.class,
            ScanRecord.class,
            ScanResult.class,
            ScanSettings.class,

            BluetoothA2dp.class,
            BluetoothAdapter.class,
            BluetoothAssignedNumbers.class,
            BluetoothClass.class,
            BluetoothDevice.class,
            BluetoothGatt.class,
            BluetoothGattCallback.class,
            BluetoothGattCharacteristic.class,
            BluetoothGattDescriptor.class,
            BluetoothGattServer.class,
            BluetoothGattServerCallback.class,
            BluetoothGattService.class,
            BluetoothHeadset.class,
            BluetoothHealth.class,
            BluetoothHealthAppConfiguration.class,
            BluetoothHealthCallback.class,
            BluetoothManager.class,
            BluetoothProfile.class,
            BluetoothServerSocket.class,
            BluetoothSocket.class
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        mTestService = Blue.createService(TestService.class);

//        BluetoothSelector.select(context, new BluetoothSelector.Callback() {
//            @Override
//            public void onDeviceSelected(BlueDevice device) {
//                Blue blue = Blue.get(device,uuid);
////                blue.setFactory();
////                blue.setGlobalCallback();
////                blue.send(bytes);
////                blue.send();
//            }
//        });
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0x7001);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (Class cls : classes) {
            if (cls.isInterface()) {
                continue;
            }
            String classStr = generateWrapperClass(cls);
            if (null != classStr) {
                saveClass(cls, classStr);
            }
        }
    }

    private void saveClass(Class cls, String classStr) {
        File file = new File(Environment.getExternalStorageDirectory(), getPackageName());
        String packageName = cls.getPackage().getName();
        packageName = packageName.replaceAll("\\.", "/");
        File classDir = new File(file.getAbsolutePath() + "/" + packageName);
        File classFile = new File(classDir, cls.getSimpleName() + ".java");
        if (!classFile.getParentFile().exists()) {
            classFile.getParentFile().mkdirs();
        }
        if (classFile.exists()) {
            classFile.delete();
        }
        try {
            classFile.createNewFile();
            FileWriter writer = new FileWriter(classFile);
            writer.write(classStr);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateWrapperClass(Class cls) {
        if (!Modifier.isAbstract(cls.getModifiers())) {
            return null;
        }
        String classStr = "package com.deparse.bluetooth.library.wrapper;\n\n";

        File dir = new File(Environment.getExternalStorageDirectory(), getApplicationContext().getPackageName());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String pkg = cls.getPackage().getName();
        File clsDir = new File(dir, pkg);
        if (!clsDir.exists()) {
            clsDir.mkdirs();
        }
        String className = cls.getSimpleName();
        classStr += format("public class %s {\n", className);
        classStr += format("private final %s mOrigin;\n", cls.getCanonicalName());
        classStr += format("public %s(%s origin){\n", className, cls.getCanonicalName());
        classStr += "this.mOrigin=origin;\n";
        classStr += "}";

        Method[] methods = cls.getMethods();
        for (Method method : methods) {
            if (!Modifier.isPublic(method.getModifiers()) || method.getDeclaringClass() == Object.class) {
                continue;
            }
            String returnType = method.getReturnType().getCanonicalName();
            String methodName = method.getName();
            String finalMethodName = methodName;
            if (Modifier.isFinal(method.getModifiers())) {
                finalMethodName = finalMethodName.substring(0, 1).toUpperCase() + finalMethodName.substring(1);
                finalMethodName = "final" + finalMethodName;
            }
            Parameter[] parameters = method.getParameters();
            String params = "";
            String paramsNames = "";
            for (Parameter parameter : parameters) {
                params += (parameter.getType().getCanonicalName() + " " + parameter.getName() + ",");
                paramsNames += (parameter.getName() + ",");
            }
            params = params.replaceAll(",$", "");
            paramsNames = paramsNames.replaceAll(",$", "");

            Class[] exceptionTypes = method.getExceptionTypes();
            String exceptions = "";
            if (null != exceptionTypes && exceptionTypes.length > 0) {
                for (Class exceptionType : exceptionTypes) {
                    exceptions += (exceptionType.getCanonicalName() + ",");
                }
            }
            exceptions = exceptions.replaceAll(",$", "");
            if (!TextUtils.isEmpty(exceptions)) {
                exceptions = "throws " + exceptions;
            }

            classStr += "\n\n";
            if (Modifier.isStatic(method.getModifiers())) {
                classStr += format("public static %s %s(%s) %s{\n", returnType, finalMethodName, params, exceptions);
                if ("void".equals(returnType)) {
                    classStr += format("%s.%s(%s);\n", cls.getCanonicalName(), methodName, paramsNames);
                } else {
                    classStr += format("return %s.%s(%s);\n", cls.getCanonicalName(), methodName, paramsNames);
                }
            } else {
                if (Modifier.isAbstract(method.getModifiers())) {
                    classStr += format("public abstract %s %s(%s) %s;\n", returnType, finalMethodName, params, exceptions);
                } else {
                    classStr += format("public %s %s(%s) %s{\n", returnType, finalMethodName, params, exceptions);
                    if ("void".equals(returnType)) {
                        classStr += format("mOrigin.%s(%s);\n", methodName, paramsNames);
                    } else {
                        classStr += format("return mOrigin.%s(%s);\n", methodName, paramsNames);
                    }
                }
            }
            classStr += "}\n";
        }
        classStr += "}\n";
        System.out.println(classStr);
        return classStr;
    }

//    private String generateWrapperClass(Class cls) {
//        if (!Modifier.isAbstract(cls.getModifiers())) {
//            return null;
//        }
//        String classStr = "package com.deparse.bluetooth.library.wrapper;\n\n";
//
//        File dir = new File(Environment.getExternalStorageDirectory(), getApplicationContext().getPackageName());
//        if (!dir.exists()) {
//            dir.mkdirs();
//        }
//        String pkg = cls.getPackage().getName();
//        File clsDir = new File(dir, pkg);
//        if (!clsDir.exists()) {
//            clsDir.mkdirs();
//        }
//        String className = cls.getSimpleName();
//        classStr += format("public class %s {\n", className);
//        classStr += format("private final %s mOrigin;\n", cls.getCanonicalName());
//        classStr += format("public %s(%s origin){\n", className, cls.getCanonicalName());
//        classStr += "this.mOrigin=origin;\n";
//        classStr += "}";
//
//        Method[] methods = cls.getMethods();
//        for (Method method : methods) {
//            if (!Modifier.isPublic(method.getModifiers()) || method.getDeclaringClass() == Object.class) {
//                continue;
//            }
//            String returnType = method.getReturnType().getCanonicalName();
//            String methodName = method.getName();
//            String finalMethodName = methodName;
//            if (Modifier.isFinal(method.getModifiers())) {
//                finalMethodName = finalMethodName.substring(0, 1).toUpperCase() + finalMethodName.substring(1);
//                finalMethodName = "final" + finalMethodName;
//            }
//            Parameter[] parameters = method.getParameters();
//            String params = "";
//            String paramsNames = "";
//            for (Parameter parameter : parameters) {
//                params += (parameter.getType().getCanonicalName() + " " + parameter.getName() + ",");
//                paramsNames += (parameter.getName() + ",");
//            }
//            params = params.replaceAll(",$", "");
//            paramsNames = paramsNames.replaceAll(",$", "");
//
//            Class[] exceptionTypes = method.getExceptionTypes();
//            String exceptions = "";
//            if (null != exceptionTypes && exceptionTypes.length > 0) {
//                for (Class exceptionType : exceptionTypes) {
//                    exceptions += (exceptionType.getCanonicalName() + ",");
//                }
//            }
//            exceptions = exceptions.replaceAll(",$", "");
//            if (!TextUtils.isEmpty(exceptions)) {
//                exceptions = "throws " + exceptions;
//            }
//
//            classStr += "\n\n";
//            if (Modifier.isStatic(method.getModifiers())) {
//                classStr += format("public static %s %s(%s) %s{\n", returnType, finalMethodName, params, exceptions);
//                if ("void".equals(returnType)) {
//                    classStr += format("%s.%s(%s);\n", cls.getCanonicalName(), methodName, paramsNames);
//                } else {
//                    classStr += format("return %s.%s(%s);\n", cls.getCanonicalName(), methodName, paramsNames);
//                }
//            } else {
//                classStr += format("public %s %s(%s) %s{\n", returnType, finalMethodName, params, exceptions);
//                if ("void".equals(returnType)) {
//                    classStr += format("mOrigin.%s(%s);\n", methodName, paramsNames);
//                } else {
//                    classStr += format("return mOrigin.%s(%s);\n", methodName, paramsNames);
//                }
//            }
//            classStr += "}\n";
//        }
//        classStr += "}\n";
//        System.out.println(classStr);
//        return classStr;
//    }

    private static String format(String fmt, Object... args) {
        return String.format(Locale.getDefault(), fmt, args);
    }

    public static void main(String... args) {
        try {
            final PipedInputStream pis = new PipedInputStream();
            final PipedOutputStream pos = new PipedOutputStream();
            pis.connect(pos);
            new Thread() {
                @Override
                public void run() {
                    DataOutputStream dos = new DataOutputStream(pos);
                    try {
                        while (true) {
                            String msg = "Writing:哈哈@" + System.currentTimeMillis();
                            System.out.println(msg);
                            dos.writeUTF(msg);
                            dos.flush();
                            Thread.sleep(100);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            new Thread() {
                @Override
                public void run() {
                    DataInputStream dis = new DataInputStream(pis);
                    try {
                        while (true) {
                            String s = dis.readUTF();
                            System.out.println("Readed:" + s);
                            Thread.sleep(1000);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        } catch (Exception e) {

        }
    }
}